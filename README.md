[![pipeline status](https://gitlab.com/tezosagora/agora/badges/master/pipeline.svg)](https://gitlab.com/tezosagora/agora/commits/master)
# Tezos Agora: Explorer for Tezos voting system

Tezos blockchain is a self-amendment system, where the amendment process is
driven by stakeholders.  It has its own voting system allowing stakeholders to
propose and vote for protocol updates.  The Tezos Agora web application is used
to monitor the voting process and its results, and to discuss existing
proposals in Tezos community.
