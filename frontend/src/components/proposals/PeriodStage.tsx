import React, { Fragment, FunctionComponent, ReactElement } from "react";
import cx from "classnames";
import styles from "~/styles/components/proposals/PeriodStage.scss";
import { PeriodType, PeriodTimeInfo } from "~/models/Period";
import AngleIcon from "~/assets/svg/AngleIcon";
import { useTranslation } from "react-i18next";
import { Link } from "react-navi";

interface PeriodStageIndicatorTypes {
  caption: string;
  isCurrent: boolean;
  link?: string;
}

const ProposalStageIndicator: FunctionComponent<PeriodStageIndicatorTypes> = ({
  caption,
  isCurrent,
  link,
}): ReactElement => {
  const className = cx(styles.proposalStage__indicator, {
    [styles.proposalStage__indicator_selected]: isCurrent,
    [styles.proposalStage__indicator_disabled]: !link && !isCurrent,
  });
  return link ? (
    <Link className={className} href={link}>
      {caption}
    </Link>
  ) : (
    <div className={className}>{caption}</div>
  );
};

interface PeriodStageTypes {
  className?: string;
  stage: PeriodType;
  periodTimes: PeriodTimeInfo;
  periodId: number;
  totalCycles: number;
  hideSelected?: boolean;
}

export const PeriodStage: FunctionComponent<PeriodStageTypes> = ({
  className,
  stage,
  periodTimes,
  periodId,
  totalCycles,
  hideSelected,
}): ReactElement => {
  const stages = [
    "proposal",
    "exploration",
    "testing",
    "promotion",
    "adoption",
  ];

  const idx = (stage: PeriodType): number =>
    stages.findIndex((s): boolean => s == stage);
  const current = idx(stage);

  const getLink = (
    s1: PeriodType,
    s2: string,
    i: number
  ): string | undefined => {
    const id = periodId - current + i;

    if (
      (!hideSelected && stage === s1) ||
      !periodTimes[id] ||
      periodTimes[id].periodType != s2
    )
      return;
    return `/period/${id}`;
  };

  // Since edo proposal has 5 cycles.
  const displayAdoptionPeriod = (): ReactElement => {
    if (totalCycles == 5)
      return (
        <Fragment>
          <AngleIcon />
          <ProposalStageIndicator
            caption="Adoption"
            link={getLink("adoption", "adoption", 4)}
            isCurrent={!hideSelected && stage === "adoption"}
          />
        </Fragment>
      );
    else return <div></div>;
  };

  return (
    <div className={className}>
      <div className={styles.proposalStage}>
        <ProposalStageIndicator
          caption="Proposal"
          link={getLink("proposal", "proposal", 0)}
          isCurrent={!hideSelected && stage === "proposal"}
        />
        <AngleIcon />
        <ProposalStageIndicator
          caption="Exploration"
          link={getLink("exploration", "testing_vote", 1)}
          isCurrent={!hideSelected && stage === "exploration"}
        />
        <AngleIcon />
        <ProposalStageIndicator
          caption={periodId > 46 ? "Cooldown" : "Testing"} // Testing period has been renamed to 'cooldown' period with Florence protocol after period 46.
          link={getLink("testing", "testing", 2)}
          isCurrent={!hideSelected && stage === "testing"}
        />
        <AngleIcon />
        <ProposalStageIndicator
          caption="Promotion"
          link={getLink("promotion", "promotion_vote", 3)}
          isCurrent={!hideSelected && stage === "promotion"}
        />
        {displayAdoptionPeriod()}
      </div>
    </div>
  );
};

export const PeriodStageShort: FunctionComponent<PeriodStageTypes> = ({
  className,
  stage,
}): ReactElement => {
  const { t } = useTranslation();

  return (
    <div className={className}>
      <div className={styles.proposalStage}>
        <ProposalStageIndicator
          caption={t(`periodType.${stage}`)}
          isCurrent={true}
        />
      </div>
    </div>
  );
};
