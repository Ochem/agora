import React, { FunctionComponent, ReactElement } from "react";
import cx from "classnames";
import styles from "~/styles/components/proposals/AdoptionCountdown.scss";
import { useTranslation } from "react-i18next";
import { DateTime } from "luxon";
import { CountdownCircleTimer } from "react-countdown-circle-timer";

interface AdoptionCountdownTypes {
  className?: string;
  dateFrom: string;
  dateTo: string;
}

const secondsToDate = (seconds: number): string => {
  const now = new Date();
  const dateInMillis = DateTime.fromJSDate(now).toMillis() + seconds * 1000;
  return DateTime.fromMillis(dateInMillis).toISO();
};

const AdoptionCountdown: FunctionComponent<AdoptionCountdownTypes> = ({
  className,
  dateFrom,
  dateTo,
}): ReactElement => {
  const { t } = useTranslation();

  const finished = DateTime.fromISO(dateTo).diffNow().milliseconds < 0;

  const from = DateTime.fromISO(dateFrom);
  const to = DateTime.fromISO(dateTo);
  const durationInSeconds = (to.toMillis() - from.toMillis()) / 1000;
  const diff = to.diffNow().milliseconds / 1000;

  return (
    <div className={cx(className, styles.countdown)}>
      <div className={styles.countdown__pieContainer}>
        <CountdownCircleTimer
          size={300}
          isPlaying
          duration={durationInSeconds}
          initialRemainingTime={diff}
          colors={"#172dF3"}
        >
          {/* eslint-disable-next-line @typescript-eslint/explicit-function-return-type */}
          {({ remainingTime }) => {
            if (remainingTime) {
              return (
                <div className={styles.countdown__text}>
                  <p>
                    {t("proposals.adoptionCountdown.remainingTime", {
                      value: {
                        date: secondsToDate(remainingTime),
                        options: {
                          largest: 2,
                        },
                      },
                    })}
                  </p>
                  <p>{t("proposals.adoptionCountdown.untilActivation")}</p>
                </div>
              );
            } else
              return (
                <div className={styles.countdown__text}>
                  <p>
                    {t("proposals.adoptionCountdown.remainingTimeFinished")}
                  </p>
                </div>
              );
          }}
        </CountdownCircleTimer>
      </div>

      <div className={styles.countdown__title}>
        {finished
          ? t("proposals.adoptionCountdown.countdownFinishedCaption")
          : t("proposals.adoptionCountdown.countdownCaption")}
      </div>
      <div className={styles.countdown__datePeriod}>
        {t("proposals.adoptionCountdown.periodDate", {
          from: {
            date: dateFrom,
            format: "cccc, MMMM d",
          },
          to: {
            date: dateTo,
            format: "cccc, MMMM d",
          },
        })}
      </div>
    </div>
  );
};

export default AdoptionCountdown;
