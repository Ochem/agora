import React, {
  FunctionComponent,
  ReactElement,
  useEffect,
  useRef,
  useState,
} from "react";
import cx from "classnames";
import ReactResizeDetector from "react-resize-detector";
import * as d3 from "d3";
import { BallotsStats, VoteStats } from "~/models/Period";
import { BarInfo, calculateBarsArray } from "~/utils/majorityGraphUtils";
import styles from "~/styles/components/proposals/graphs/MajorityGraph.scss";
import { useTranslation } from "react-i18next";

const grpahBackgroundColor = "rgba(44, 125, 247, 0.1)";
const markColor = "#123262";
const captionDistance = 8;
const barHeightMobile = 16;
const barHeightDesktop = 24;
const animationDuration = 600;

type MajorityGraphType = "quorum" | "majority";
type UpdateGraphCallback = (width: number, newBarArray: BarInfo[]) => void;
type UpdateCaptionCallback = (caption: string) => void;

interface GraphTypes {
  width: number;
  ballotsStats: BallotsStats;
  voteStats: VoteStats;
  type: MajorityGraphType;
  markCaption: string;
  markValue: number;
}

const drawGraph = (
  graph: SVGSVGElement,
  graphSettings: GraphTypes
): [((width: number, arg: BarInfo[]) => void)[], UpdateCaptionCallback] => {
  const {
    width,
    ballotsStats,
    voteStats,
    type,
    markCaption,
    markValue,
  } = graphSettings;

  const barArray = calculateBarsArray(width, ballotsStats, voteStats);

  const svg = d3.select(graph).attr("width", "100%");

  const graphWrapper = svg.append("svg");
  const graphBar = graphWrapper.append("svg").attr("y", "32");
  const graphBarMain = graphBar.append("svg");
  const updateFunctions: UpdateGraphCallback[] = [];

  // Main clipping object for everything on bar including background.
  const mainClipPath = graphBar
    .append("clipPath")
    .attr("id", `bar-${type}`)
    .append("rect")
    .attr("width", width);

  // Creating background
  const backgroundBar = graphBarMain
    .append("rect")
    .attr("x", 0)
    .attr("width", width)
    .attr("fill", grpahBackgroundColor)
    .attr("clip-path", `url(#bar-${type})`);

  // Add clip path for yay, nay, and pass
  const barClipPath = graphBarMain
    .append("clipPath")
    .attr("id", `mainBar-${type}`)
    .append("rect")
    .attr("width", barArray.length > 0 ? barArray[barArray.length - 1].endX : 0)
    .attr("clip-path", `url(#bar-${type})`);

  // Draw yay, nay, pass, and white dividers
  const bars = barArray.map(
    (item, index): d3.Selection<SVGRectElement, unknown, null, undefined> => {
      const rect = graphBarMain.append("rect").attr("fill", item.color);
      rect
        .transition()
        .duration(animationDuration)
        .attr("x", item.startX)
        .attr("width", item.endX - item.startX);
      if (item.color !== "white") {
        rect.attr("clip-path", `url(#mainBar-${type})`);
      }

      // Add function to update svg on resize
      updateFunctions.push((width: number, newBarArray: BarInfo[]): void => {
        if (index < newBarArray.length) {
          rect
            .attr("x", newBarArray[index].startX)
            .attr("width", newBarArray[index].endX - newBarArray[index].startX);
        }
      });

      return rect;
    }
  );

  const updateGraph = (): void => {
    const isMobile = window.innerWidth <= 850;
    const barHeight = isMobile ? barHeightMobile : barHeightDesktop;

    svg.attr("height", isMobile ? 48 : 56);
    mainClipPath.attr("height", barHeight).attr("rx", barHeight / 2);

    backgroundBar.attr("height", barHeight);

    barClipPath.attr("height", barHeight).attr("rx", barHeight / 2);

    bars.forEach((bar): void => {
      bar.attr("height", barHeight);
    });
  };

  updateGraph();

  // Mark line with caption
  const markLine = graphWrapper
    .append("line")
    .style("stroke", markColor)
    .style("stroke-width", 2)
    .style("opacity", 0.1)
    .attr("x1", (width * markValue) / 100)
    .attr("y1", 0)
    .attr("x2", (width * markValue) / 100)
    .attr("y2", 20);
  const textElement = graphWrapper.append("text");
  const markText = textElement
    .style("color", markColor)
    .style("opacity", 0.5)
    .style("font-size", 14)
    .style("font-weight", 500)
    .attr("height", 20)
    .text(markCaption)
    .attr("dominant-baseline", "hanging")
    .attr("y", 4)
    .attr("text-anchor", function(): string {
      const textWidth = this.getComputedTextLength();
      if (textWidth > (width * (100 - markValue)) / 100 - captionDistance) {
        return "end";
      }
      return "start";
    })
    .attr("x", function(): number {
      const textWidth = this.getComputedTextLength();
      if (textWidth > (width * (100 - markValue)) / 100 - captionDistance) {
        return (width * markValue) / 100 - captionDistance;
      }
      return (width * markValue) / 100 + captionDistance;
    });

  updateFunctions.push((width: number, newBarArray: BarInfo[]): void => {
    updateGraph();

    mainClipPath.attr("width", width);
    backgroundBar.attr("width", width);
    barClipPath.attr(
      "width",
      newBarArray.length > 0 ? newBarArray[newBarArray.length - 1].endX : 0
    );
    markLine
      .attr("x1", (width * markValue) / 100)
      .attr("x2", (width * markValue) / 100);

    // Drawing caption to left or to right of mark line.
    markText
      .attr("text-anchor", function(): string {
        const textWidth = this.getComputedTextLength();
        if (textWidth > (width * (100 - markValue)) / 100 - captionDistance) {
          return "end";
        }
        return "start";
      })
      .attr("x", function(): number {
        const textWidth = this.getComputedTextLength();
        if (textWidth > (width * (100 - markValue)) / 100 - captionDistance) {
          return (width * markValue) / 100 - captionDistance;
        }
        return (width * markValue) / 100 + captionDistance;
      });
  });
  return [
    updateFunctions,
    (caption: string): void => {
      if (caption && type == "quorum") {
        textElement.text(caption);
      }
    },
  ];
};

const Graph: FunctionComponent<GraphTypes> = (props): ReactElement => {
  const { width, ballotsStats, voteStats } = props;
  const graph = useRef<SVGSVGElement>(null);
  const [initQuorum, setQuorum] = useState(ballotsStats.quorum);
  const [hasBeenDrawed, setDrawed] = useState(false);
  const [updateGraph, setUpdateGraph] = useState<
    ((width: number, newBarInfo: BarInfo[]) => void)[]
  >();

  // For some reason, this only works when the function is wrapped
  // in an object.
  const [updateCaption, setUpdateCaption] = useState<{
    callback: (caption: string) => void;
  }>();

  const { t } = useTranslation();

  useEffect((): void => {
    if (initQuorum != ballotsStats.quorum) {
      const newCaption = t("proposals.majorityGraph.quorumCaption", {
        value: ballotsStats.quorum.toString(),
      });
      if (updateCaption) updateCaption.callback(newCaption);
      setQuorum(ballotsStats.quorum);
    }
  }, [ballotsStats]);

  useEffect((): void => {
    if (width === 0 || !graph.current || hasBeenDrawed) return;
    const [updateFunctions, captionUpdate] = drawGraph(graph.current, props);
    setUpdateGraph(updateFunctions);
    setUpdateCaption({ callback: captionUpdate });
    setDrawed(true);
  }, [width, ballotsStats]);

  useEffect((): void => {
    if (!updateGraph) return;
    const barArray = calculateBarsArray(width, ballotsStats, voteStats);
    updateGraph.forEach((item: UpdateGraphCallback): void => {
      item(width, barArray);
    });
  }, [width, ballotsStats]);
  return <svg ref={graph} />;
};

interface MajorityGraphTypes {
  className?: string;
  ballotsStats: BallotsStats;
  voteStats: VoteStats;
}

const MajorityGraph: FunctionComponent<MajorityGraphTypes> = ({
  className,
  ballotsStats,
  voteStats,
}): ReactElement => {
  const majorityBallots: BallotsStats = {
    ...ballotsStats,
    pass: 0,
  };
  const majorityVotes: VoteStats = {
    votesAvailable: majorityBallots.yay + majorityBallots.nay,
    votesCast: majorityBallots.yay + majorityBallots.nay,
    numVoters: voteStats.numVoters,
    numVotersTotal: voteStats.numVotersTotal,
  };

  const { t } = useTranslation();

  return (
    <div className={cx(className, styles.majorityGraph)}>
      <ReactResizeDetector
        handleWidth
        render={({ width = 0 }): ReactElement => (
          <>
            <Graph
              width={width}
              ballotsStats={majorityBallots}
              voteStats={majorityVotes}
              type="majority"
              markCaption={t("proposals.majorityGraph.supermajorityCaption", {
                value: ballotsStats.supermajority,
              })}
              markValue={ballotsStats.supermajority}
            />
            <Graph
              width={width}
              ballotsStats={ballotsStats}
              voteStats={voteStats}
              type="quorum"
              markCaption={t("proposals.majorityGraph.quorumCaption", {
                value: ballotsStats.quorum,
              })}
              markValue={ballotsStats.quorum}
            />
          </>
        )}
      />
    </div>
  );
};

export default MajorityGraph;
