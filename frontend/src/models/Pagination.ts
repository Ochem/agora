export interface Pagination {
  total: number;
  limit: number;
  rest: number;
  lastId: number;
}
