import React, {
  FunctionComponent,
  ReactElement,
  useState,
  useEffect,
} from "react";
import { PromotionPeriodInfo } from "~/models/Period";
import { LayoutContent } from "~/components/common/Layout";
import styles from "~/styles/pages/proposals/PromotionStagePage.scss";
import BakersFilter from "~/components/proposals/table/BakersFilter";
import BakersTable from "~/components/proposals/table/BakersTable";
import { useTranslation } from "react-i18next";
import ProposalDescription from "~/components/proposals/ProposalDescription";
import { useSelector } from "react-redux";
import { ProposalBallotsList } from "~/models/ProposalBallotsList";
import { RootStoreType } from "~/store";
import { fetchRestBallots } from "~/store/actions/periodActions";
import { PeriodState } from "~/store/reducers/periodReducer";
import { Decision } from "~/models/Decision";
import MajorityGraph from "~/components/proposals/graphs/MajorityGraph";
import ParticipationTracker from "~/components/proposals/ParticipationTracker";
import NonVotersTable from "~/components/proposals/table/NonVotersTable";
import { Proposer } from "~/models/ProposalInfo";

interface PromotionViewProps {
  period: PromotionPeriodInfo;
}

const PromotionView: FunctionComponent<PromotionViewProps> = ({
  period,
}): ReactElement => {
  const { t } = useTranslation();
  const [inverted, setInverted] = useState(false);
  const loading: boolean = useSelector(
    (state: RootStoreType): boolean => state.periodStore.ballotsLoading
  );
  const initialBallots: ProposalBallotsList | null = useSelector(
    (state: RootStoreType): ProposalBallotsList | null => {
      if (state.periodStore.ballots) {
        return {
          pagination: state.periodStore.ballots.pagination,
          results: state.periodStore.ballots.data,
        };
      }
      return null;
    }
  );
  const initialNonVoters: Proposer[] = useSelector(
    (state: RootStoreType): Proposer[] => {
      return state.periodStore.nonVoters || [];
    }
  );

  const initialDecisions = useSelector((state: RootStoreType): Decision[] => {
    return state.periodStore.ballotsDecisions;
  });
  const store = useSelector(
    (state: RootStoreType): PeriodState => state.periodStore
  );

  const initialPeriodId = useSelector((state: RootStoreType): number | null => {
    if (state.periodStore.period) return state.periodStore.period.period.id;
    else return null;
  });

  const [ballots, setBallots] = useState(initialBallots);
  const [nonVoters, setNonVoters] = useState(initialNonVoters.slice(0, 10));
  const [decisions, setDecisions] = useState(initialDecisions);
  const [loadedPeriodId, setLoadedPeriodId] = useState(initialPeriodId);

  useEffect((): void => {
    if (initialPeriodId !== loadedPeriodId) {
      // We check if we are still the period we are supposed to be in (initialPeriodId)
      // If we are not, then reset the loaded Ballots and nonVoters with the corresponding
      // entries from root store, and update the loadedPeriodId.
      setBallots(initialBallots);
      setNonVoters(initialNonVoters);
      setLoadedPeriodId(initialPeriodId);
    }
  }, [initialPeriodId, loadedPeriodId]);

  const hasMore = (): boolean => {
    if (inverted) return nonVoters.length != initialNonVoters.length;
    return ballots ? ballots.pagination.rest > 0 : false;
  };

  const handleShowAllBallots = (): void => {
    fetchRestBallots(store).then((result): void => {
      if (!result || !ballots) return;
      setBallots({
        pagination: result.payload.pagination,
        results: [...ballots.results, ...result.payload.results],
      });
    });
  };
  const handleShowAllNonVoters = (): void => {
    setNonVoters(initialNonVoters);
  };

  const handleFilterChange = (newValue: Decision[]): void => {
    if (ballots && ballots.pagination.rest) handleShowAllBallots();
    setDecisions(newValue);
  };

  const handleSortChange = (): void => {
    if (ballots && ballots.pagination.rest) handleShowAllBallots();
  };

  return (
    <>
      <LayoutContent className={styles.period__primaryInfo}>
        <div>
          <ProposalDescription
            className={styles.promotion__description}
            title={
              period.proposal.title
                ? period.proposal.title
                : period.proposal.hash
            }
            description={
              period.proposal.shortDescription
                ? period.proposal.shortDescription
                : t("proposals.common.noDescriptionCaption")
            }
            discourseLink={period.proposal.discourseLink}
            learnMoreLink={`/proposal/${period.proposal.hash}`}
          />
          <div className={styles.promotion__voters}>
            <MajorityGraph
              className={styles.promotion__voters__graph}
              ballotsStats={period.ballots}
              voteStats={period.voteStats}
            />
            <ParticipationTracker
              voteStats={period.voteStats}
              onInvert={setInverted}
              hideProgressBar
            />
          </div>
        </div>
      </LayoutContent>
      <LayoutContent className={styles.period__secondaryInfo}>
        {!loading && ballots ? (
          <>
            {inverted ? (
              <NonVotersTable
                data={nonVoters}
                className={styles.bakers__table}
              />
            ) : (
              <>
                <BakersFilter
                  className={styles.bakers__filter}
                  ballots={period.ballots}
                  filter={initialDecisions}
                  onFilterChange={handleFilterChange}
                />
                <BakersTable
                  data={ballots.results.filter(
                    (i): boolean =>
                      !decisions.length || decisions.includes(i.decision)
                  )}
                  className={styles.bakers__table}
                  onSortChange={handleSortChange}
                />
              </>
            )}
            {hasMore() && (
              <button
                className={styles.bakers__showAllButton}
                onClick={
                  inverted ? handleShowAllNonVoters : handleShowAllBallots
                }
              >
                {t("common.showAll")}
              </button>
            )}
          </>
        ) : null}
      </LayoutContent>
    </>
  );
};

export default PromotionView;
