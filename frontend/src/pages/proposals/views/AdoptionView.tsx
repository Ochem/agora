import React, { FunctionComponent, ReactElement } from "react";
import { AdoptionPeriodInfo } from "~/models/Period";
import { LayoutContent } from "~/components/common/Layout";
import styles from "~/styles/pages/proposals/AdoptionStagePage.scss";
import ProposalDescription from "~/components/proposals/ProposalDescription";
import ProposalDescriptionCard from "~/components/proposals/ProposalDescriptionCard";
import { useTranslation } from "react-i18next";
import AdoptionCountdown from "../../../components/proposals/AdoptionCountdown";

interface AdoptionViewProps {
  period: AdoptionPeriodInfo;
}

const AdoptionView: FunctionComponent<AdoptionViewProps> = ({
  period,
}): ReactElement => {
  const { t } = useTranslation();
  return (
    <>
      <LayoutContent className={styles.period__primaryInfo}>
        <div>
          <ProposalDescription
            className={styles.testing__description}
            title={
              period.proposal.title
                ? period.proposal.title
                : period.proposal.hash
            }
            description={
              period.proposal.shortDescription
                ? period.proposal.shortDescription
                : t("proposals.common.noDescriptionCaption")
            }
            discourseLink={period.proposal.discourseLink}
            learnMoreLink={`/proposal/${period.proposal.hash}`}
          />
          <AdoptionCountdown
            className={styles.testing__countdown}
            dateFrom={period.period.startTime}
            dateTo={period.period.endTime}
          />
        </div>
      </LayoutContent>
      <LayoutContent className={styles.period__secondaryInfo}>
        <ProposalDescriptionCard
          className={styles.testing__proposalCard}
          content={
            period.proposal.longDescription
              ? period.proposal.longDescription
              : t("proposals.common.noDescriptionCaption")
          }
        />
      </LayoutContent>
    </>
  );
};

export default AdoptionView;
