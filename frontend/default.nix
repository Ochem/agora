{ buildNpmPackage, fetchurl, utillinux }:

buildNpmPackage {
  src = ./.;

  extraEnvVars.SASS_BINARY_PATH = fetchurl {
    url = "https://github.com/sass/node-sass/releases/download/v4.12.0/linux-x64-72_binding.node";
    sha256 = "1yabm9g0pfxmq6xw3zdkbysc001mihpkwzyn96flvld64nli57qs";
  };

  buildInputs = [
    utillinux # lscpu for parcel
  ];

  doCheck = true;
  checkPhase = ''
    npm run tslint
    npm run stylelint
    npm run tscompile
    npm run test
  '';

  npmBuild = ''
    npm run build
  '';

  installPhase = ''
    mv dist $out
  '';
}
