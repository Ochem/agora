{ pkgs }: with pkgs;

let
  # Keep in mind that parent or global .gitignore are not respected
  source = ./.;

  project = pkgs.callPackage ./. { };
in
{
  agora-frontend = project;
}
