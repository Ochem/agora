swagger: '2.0'
externalDocs:
  url: http://swagger.io
  description: Find out more about Swagger
schemes:
- http
- https
info:
  contact:
    email: hi@serokell.io
    url: https://serokell.io
    name: Serokell OÜ
  version: 1.0.0
  title: Tezos Agora API
definitions:
  PaginationData:
    required:
    - rest
    type: object
    properties:
      lastId:
        maximum: 4294967295
        minimum: 0
        type: integer
      limit:
        $ref: '#/definitions/Limit'
      rest:
        $ref: '#/definitions/Amount'
  Amount:
    maximum: 4294967295
    minimum: 0
    type: integer
    description: Number of rest entries
  ProposalVote:
    required:
    - id
    - proposal
    - author
    - operation
    - timestamp
    type: object
    properties:
      proposalTitle:
        type: string
      operation:
        $ref: '#/definitions/Hash'
      proposal:
        $ref: '#/definitions/Hash'
      author:
        $ref: '#/definitions/Baker'
      id:
        $ref: '#/definitions/ProposalVoteId'
      timestamp:
        $ref: '#/definitions/UTCTime'
  Hash:
    format: byte
    type: string
    description: Base58 hash value
  Period:
    required:
    - id
    - startLevel
    - curLevel
    - endLevel
    - startTime
    - endTime
    - cycle
    - totalCycles
    type: object
    properties:
      curLevel:
        $ref: '#/definitions/Level'
      startTime:
        $ref: '#/definitions/UTCTime'
      endLevel:
        $ref: '#/definitions/Level'
      endTime:
        $ref: '#/definitions/UTCTime'
      id:
        $ref: '#/definitions/PeriodId'
      cycle:
        $ref: '#/definitions/Cycle'
      startLevel:
        $ref: '#/definitions/Level'
      totalCycles:
        maximum: 4294967295
        minimum: 0
        type: integer
  Ballot:
    required:
    - id
    - author
    - decision
    - operation
    - timestamp
    type: object
    properties:
      operation:
        $ref: '#/definitions/Hash'
      author:
        $ref: '#/definitions/Baker'
      id:
        $ref: '#/definitions/BallotId'
      decision:
        $ref: '#/definitions/Decision'
      timestamp:
        $ref: '#/definitions/UTCTime'
  Votes:
    maximum: 2147483647
    minimum: -2147483648
    type: integer
    description: Number of votes
  PeriodInfo:
    minProperties: 1
    maxProperties: 1
    type: object
    properties:
      explorationInfo:
        required:
        - period
        - totalPeriods
        - periodTimes
        - proposal
        - voteStats
        - ballots
        - discourseLink
        type: object
        properties:
          period:
            $ref: '#/definitions/Period'
          advanced:
            type: boolean
          totalPeriods:
            maximum: 4294967295
            minimum: 0
            type: integer
          proposal:
            $ref: '#/definitions/Proposal'
          discourseLink:
            type: string
          ballots:
            $ref: '#/definitions/Ballots'
          periodTimes:
            items:
              $ref: '#/definitions/PeriodItemInfo'
            type: array
          voteStats:
            $ref: '#/definitions/VoteStats'
      proposalInfo:
        required:
        - period
        - totalPeriods
        - periodTimes
        - voteStats
        - discourseLink
        type: object
        properties:
          period:
            $ref: '#/definitions/Period'
          totalPeriods:
            maximum: 4294967295
            minimum: 0
            type: integer
          discourseLink:
            type: string
          periodTimes:
            items:
              $ref: '#/definitions/PeriodItemInfo'
            type: array
          voteStats:
            $ref: '#/definitions/VoteStats'
          winner:
            $ref: '#/definitions/Proposal'
      promotionInfo:
        required:
        - period
        - totalPeriods
        - periodTimes
        - proposal
        - voteStats
        - ballots
        - discourseLink
        type: object
        properties:
          period:
            $ref: '#/definitions/Period'
          advanced:
            type: boolean
          totalPeriods:
            maximum: 4294967295
            minimum: 0
            type: integer
          proposal:
            $ref: '#/definitions/Proposal'
          discourseLink:
            type: string
          ballots:
            $ref: '#/definitions/Ballots'
          periodTimes:
            items:
              $ref: '#/definitions/PeriodItemInfo'
            type: array
          voteStats:
            $ref: '#/definitions/VoteStats'
      testingInfo:
        required:
        - period
        - totalPeriods
        - periodTimes
        - proposal
        - discourseLink
        type: object
        properties:
          period:
            $ref: '#/definitions/Period'
          advanced:
            type: boolean
          totalPeriods:
            maximum: 4294967295
            minimum: 0
            type: integer
          proposal:
            $ref: '#/definitions/Proposal'
          discourseLink:
            type: string
          periodTimes:
            items:
              $ref: '#/definitions/PeriodItemInfo'
            type: array
      adoptionInfo:
        required:
        - period
        - totalPeriods
        - periodTimes
        - proposal
        - discourseLink
        type: object
        properties:
          period:
            $ref: '#/definitions/Period'
          totalPeriods:
            maximum: 4294967295
            minimum: 0
            type: integer
          proposal:
            $ref: '#/definitions/Proposal'
          discourseLink:
            type: string
          periodTimes:
            items:
              $ref: '#/definitions/PeriodItemInfo'
            type: array
  PeriodItemInfo:
    required:
    - startTime
    - endTime
    - periodType
    type: object
    properties:
      startTime:
        $ref: '#/definitions/UTCTime'
      endTime:
        $ref: '#/definitions/UTCTime'
      periodType:
        $ref: '#/definitions/PeriodType'
  Proposal:
    required:
    - period
    - hash
    - timeCreated
    - proposer
    - votesCasted
    - votersNum
    - minQuorum
    type: object
    properties:
      longDescription:
        type: string
      votesCasted:
        $ref: '#/definitions/Votes'
      hash:
        $ref: '#/definitions/Hash'
      period:
        $ref: '#/definitions/PeriodId'
      minQuorum:
        $ref: '#/definitions/Quorum'
      proposer:
        $ref: '#/definitions/Baker'
      shortDescription:
        type: string
      votersNum:
        $ref: '#/definitions/Voters'
      proposalFile:
        type: string
      timeCreated:
        $ref: '#/definitions/UTCTime'
      title:
        type: string
      discourseLink:
        type: string
  Baker:
    required:
    - pkh
    - rolls
    - name
    type: object
    properties:
      logoUrl:
        type: string
      pkh:
        $ref: '#/definitions/Hash'
      name:
        type: string
      profileUrl:
        type: string
      rolls:
        $ref: '#/definitions/Rolls'
  PaginatedList:
    required:
    - pagination
    - results
    type: object
    properties:
      pagination:
        $ref: '#/definitions/PaginationData'
      results:
        items:
          $ref: '#/definitions/Ballot'
        type: array
  UTCTime:
    example: 2016-07-22T00:00:00Z
    format: yyyy-mm-ddThh:MM:ssZ
    type: string
  Limit:
    maximum: 4294967295
    minimum: 0
    type: integer
    description: Requested number of entries to return
  BallotId:
    maximum: 2147483647
    minimum: -2147483648
    type: integer
    description: Ballot id
  PeriodType:
    enum:
    - proposal
    - testing_vote
    - testing
    - promotion_vote
    - adoption
    description: Period type
  Cycle:
    maximum: 2147483647
    minimum: -2147483648
    type: integer
    description: Cycle number
  PeriodId:
    maximum: 2147483647
    minimum: -2147483648
    type: integer
    description: Period number
  Decision:
    enum:
    - yay
    - nay
    - pass
    description: Ballot decision
  Ballots:
    required:
    - yay
    - nay
    - pass
    - quorum
    - supermajority
    type: object
    properties:
      nay:
        $ref: '#/definitions/Votes'
      supermajority:
        format: float
        type: number
      quorum:
        format: float
        type: number
      pass:
        $ref: '#/definitions/Votes'
      yay:
        $ref: '#/definitions/Votes'
  Rolls:
    maximum: 2147483647
    minimum: -2147483648
    type: integer
    description: Number of rolls
  Level:
    maximum: 2147483647
    minimum: -2147483648
    type: integer
    description: Block level
  Voters:
    maximum: 2147483647
    minimum: -2147483648
    type: integer
    description: Number of voters
  ProposalVoteId:
    maximum: 2147483647
    minimum: -2147483648
    type: integer
    description: Proposal vote id
  VoteStats:
    required:
    - votesCast
    - votesAvailable
    - numVoters
    - numVotersTotal
    type: object
    properties:
      numVoters:
        $ref: '#/definitions/Voters'
      votesAvailable:
        $ref: '#/definitions/Votes'
      numVotersTotal:
        $ref: '#/definitions/Voters'
      votesCast:
        $ref: '#/definitions/Votes'
  Quorum:
    maximum: 2147483647
    minimum: -2147483648
    type: integer
    description: A wrapper for Int32 used to represent Quorum values
paths:
  /api/v1/non_voters/{period_id}:
    get:
      summary: Bakers who didn't cast their vote so far.
      responses:
        '404':
          description: '`period_id` not found'
        '200':
          schema:
            items:
              $ref: '#/definitions/Baker'
            type: array
          description: ''
      produces:
      - application/json;charset=utf-8
      parameters:
      - maximum: 2147483647
        format: int32
        minimum: -2147483648
        required: true
        in: path
        name: period_id
        type: integer
  /api/v1/ballots/{period_id}:
    get:
      summary: Ballots for given voting period.
      responses:
        '404':
          description: '`period_id` not found'
        '400':
          description: Invalid `decisions` or `limit` or `lastId`
        '200':
          schema:
            $ref: '#/definitions/PaginatedList'
          description: ''
      produces:
      - application/json;charset=utf-8
      parameters:
      - maximum: 2147483647
        format: int32
        minimum: -2147483648
        required: true
        in: path
        name: period_id
        type: integer
      - maximum: 2147483647
        format: int32
        minimum: -2147483648
        required: false
        in: query
        name: lastId
        type: integer
      - maximum: 4294967295
        minimum: 0
        required: false
        in: query
        name: limit
        type: integer
      - items:
          type: string
          enum:
          - yay
          - nay
          - pass
        required: false
        in: query
        name: decisions
        collectionFormat: null
        type: array
  /api/v1/proposal/{proposal_id}/votes:
    get:
      summary: Proposal votes issued for a given proposal
      responses:
        '404':
          description: '`proposal_id` not found'
        '400':
          description: Invalid `limit` or `lastId`
        '200':
          schema:
            $ref: '#/definitions/PaginatedList'
          description: ''
      produces:
      - application/json;charset=utf-8
      parameters:
      - required: true
        in: path
        name: proposal_id
        type: string
      - maximum: 2147483647
        format: int32
        minimum: -2147483648
        required: false
        in: query
        name: lastId
        type: integer
      - maximum: 4294967295
        minimum: 0
        required: false
        in: query
        name: limit
        type: integer
  /api/v1/period:
    get:
      summary: Info about given voting period
      responses:
        '400':
          description: Invalid `id`
        '200':
          schema:
            $ref: '#/definitions/PeriodInfo'
          description: ''
      produces:
      - application/json;charset=utf-8
      parameters:
      - maximum: 2147483647
        format: int32
        minimum: -2147483648
        required: false
        in: query
        name: id
        type: integer
  /api/v1/proposal/{proposal_id}:
    get:
      summary: Info about specific proposal
      responses:
        '404':
          description: '`proposal_id` not found'
        '200':
          schema:
            $ref: '#/definitions/Proposal'
          description: ''
      produces:
      - application/json;charset=utf-8
      parameters:
      - required: true
        in: path
        name: proposal_id
        type: string
  /api/v1/proposal_votes/{period_id}:
    get:
      summary: Proposal votes for given proposal period.
      responses:
        '404':
          description: '`period_id` not found'
        '400':
          description: Invalid `limit` or `lastId`
        '200':
          schema:
            $ref: '#/definitions/PaginatedList'
          description: ''
      produces:
      - application/json;charset=utf-8
      parameters:
      - maximum: 2147483647
        format: int32
        minimum: -2147483648
        required: true
        in: path
        name: period_id
        type: integer
      - maximum: 2147483647
        format: int32
        minimum: -2147483648
        required: false
        in: query
        name: lastId
        type: integer
      - maximum: 4294967295
        minimum: 0
        required: false
        in: query
        name: limit
        type: integer
  /api/v1/proposals/{period_id}:
    get:
      summary: Proposals for given proposal period.
      responses:
        '404':
          description: '`period_id` not found'
        '200':
          schema:
            items:
              $ref: '#/definitions/Proposal'
            type: array
          description: ''
      produces:
      - application/json;charset=utf-8
      parameters:
      - maximum: 2147483647
        format: int32
        minimum: -2147483648
        required: true
        in: path
        name: period_id
        type: integer
