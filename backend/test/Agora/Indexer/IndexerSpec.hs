module Agora.Indexer.IndexerSpec
      ( spec
      ) where

import qualified Data.Set as Set
import Test.Hspec (Spec, describe, it, shouldBe)

import Agora.Discourse.Types
import Agora.Indexer.Capability
import Agora.Indexer.Common
import Agora.Indexer.Handlers
import qualified Agora.Indexer.Types as IT
import qualified Agora.Types as AT
import Agora.Util
import qualified Agora.Web.Types as WT

spec :: Spec
spec =
  describe "Indexer" $ do
    it "can generate periodInfo" $ do
      let expectationFile = "resources/indexer/test-expectations/getPeriodInfo/getPeriodInfo.json"
      (runWithMocks $ getPeriodInfo Nothing) >>= checkExpectationFileOrWrite expectationFile

    it "can generate periodInfos" $ runWithMocks $ flip mapM_ [0..(43 :: Integer)] $ \periodId -> do
      let expectationFile = (afp "resources/indexer/test-expectations/getPeriodInfo/getPeriodInfo" periodId) <>".json"
      (getPeriodInfo (Just $ AT.Id $ fromInteger periodId)) >>= liftIO . checkExpectationFileOrWrite expectationFile

    it "can generate proposals for period" $ runWithMocks $ flip mapM_ [0..(43 :: Integer)] $ \periodId -> do
      let expectationFile = (afp "resources/indexer/test-expectations/getProposalsByPeriod/getProposals" periodId) <>".json"
      getProposals (AT.Id $ fromInteger periodId) >>= liftIO . checkExpectationFileOrWrite expectationFile

    it "can generate proposal for hash" $ runWithMocks $ flip mapM_ proposalHashes $ \pHash -> do
      let expectationFile = (afp "resources/indexer/test-expectations/getProposalsByHash/getProposal" pHash) <>".json"
      getProposal pHash  >>= liftIO . checkExpectationFileOrWrite expectationFile

    it "can generate proposal votes for period" $ runWithMocks $ flip mapM_ [0..(43 :: Integer)] $ \periodId -> do
      let expectationFile = (afp "resources/indexer/test-expectations/getProposalVotes/getProposalVotesForPeriod" periodId) <>".json"
      getProposalVotes (AT.Id $ fromInteger periodId) >>= liftIO . checkExpectationFileOrWrite expectationFile

    it "can generate specific proposal votes for proposal hash" $ runWithMocks $ flip mapM_ proposalHashes $ \pHash -> do
      let expectationFile = (afp "resources/indexer/test-expectations/getSpecificProposalVotes/getSpecificProposalVotes" pHash) <>".json"
      getSpecificProposalVotes pHash >>= liftIO . checkExpectationFileOrWrite expectationFile

    it "can generate ballots" $ runWithMocks $ flip mapM_ [0..(43 :: Integer)] $ \periodId -> do
      let expectationFile = (afp "resources/indexer/test-expectations/getBallots/getBallotsForPeriod" periodId) <>".json"
      getBallots (AT.Id $ fromInteger periodId) >>= liftIO . checkExpectationFileOrWrite expectationFile

    it "can generate non-voters" $ runWithMocks $ flip mapM_ [0..(43 :: Integer)] $ \periodId -> do
      let expectationFile = (afp "resources/indexer/test-expectations/getNonVoters/getNonVotersForPeriod" periodId) <>".json"
      getNonVoters (AT.Id $ fromInteger periodId) >>= liftIO . checkExpectationFileOrWrite expectationFile

    it "can post discourse" $ do
      (_, topics, _) <- runWithMocksReturnDiscourse $ pure ()
      let expected = Set.fromList $
            [ "(PsDELPH1K) Manually made" -- This is set to be an existing topic for which agora should not create a duplicate
            , "(Psd1ynUBh)"
            , "(Pt24m4xiP)"
            , "(PtdRxBHvc)"
            , "(PsBABY5nk)"
            , "(PsBABY5HQ)"
            , "(PtCarthav)"
            , "(PsCARTHAG)"
            , "(PtEdoTezd)"
            , "(PsFLorBAr)"
            , "(PsFLorena)"
            ]
      expected `shouldBe` (Set.fromList ((unTitle . tTitle) <$> topics))


    -- Customized tests to target specific cases
    --
    -- The following appear to be the minimal mock data that is required to successfuly construct
    -- a single period info. The hope is that the following can be modified comparitively easily
    -- to re-construct special indexer states in future tests.
    --
    -- A note about mocking paged endpoints: When mocking paged endpoint of the indexer, remember
    -- to return non-empty data only in the first page. Instead if you return non-empty result for
    -- all requests, the `paged` function just goes into an endless loop.

    it "can generate periodInfo and proposal details for ongoing proposal periods" $ do
      let dummyTime = parseUTCTime "2021-06-04T11:27:03Z"
      let mockProtocolHash = AT.encodeHash "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
      let mockPeriodInfo = IT.PeriodInfo
            { kind = IT.Proposing
            , firstLevel = 400
            , lastLevel = 4096
            , startTime = dummyTime
            , endTime = dummyTime
            , status = Nothing
            , epoch = 0
            , yayBallots = Nothing
            , yayRolls = Nothing
            , nayBallots    = Nothing
            , nayRolls      = Nothing
            , passBallots   = Nothing
            , passRolls     = Nothing
            , ballotsQuorum = Nothing
            , supermajority = Nothing
            , totalBakers   = Nothing
            , totalRolls    = Nothing
            , index = 0
            }
      let mockProtocol = IT.Protocol
            { hash = mockProtocolHash
            , firstLevel = 200
            , lastLevel = Nothing
            , constants = IT.Constants
                { proposalQuorum = Just 5
                , blocksPerCycle = Just 4096
                , blocksPerVoting = Just 32768
                }
            }
      let mockDelegate = IT.Delegate
            { address = AT.encodeHash "tz1fJHFn6sWEd3NnBPngACuw2dggTv6nQZ7g" -- This address has to be from the "bakers.json" file in the resources directory.
            , alias = Nothing
            , logoUrl = Nothing
            }

      let mockOperation = IT.Operation
            { timestamp = dummyTime
            , level = 250
            , hash = AT.encodeHash "BKxssbv2J7oqhHU3cZDfbzXPXvpAnz3rgSCxG2jT2sdeMfAKbj2"
            , proposal = Nothing
            , delegate = mockDelegate
            }

      let mockProposal = IT.Proposal
                { epoch = 1
                , rolls = 0
                , hash = AT.encodeHash "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
                , firstPeriod =  0
                , lastPeriod = 0
                , initiator = mockDelegate
                , status = IT.Active
                }

      let mockProposalOperation = IT.ProposalOperation
            { id = 100
            , hash = AT.encodeHash "BKxssbv2J7oqhHU3cZDfbzXPXvpAnz3rgSCxG2jT2sdeMfAKbj2"
            , proposal = IT.SimpleProposal Nothing (IT.proposalHash mockProposal)
            , timestamp = dummyTime
            , rolls = fromInteger 10
            , delegate = mockDelegate
            }

      let indxEp = IndexerEndpointsIO
            { neLevel = \_ _ -> do
                pure $ IT.Block
                  { level = 600
                  , timestamp = parseUTCTime "2021-06-04T11:27:03Z"
                  }
            , neHead = pure $ IT.Head
                { hash = AT.encodeHash "BKxssbv2J7oqhHU3cZDfbzXPXvpAnz3rgSCxG2jT2sdeMfAKbj2"
                , level = 600
                , protocol = AT.encodeHash "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
                , timestamp = parseUTCTime "2021-06-04T11:27:03Z"
                }
            , neBallotsForProposal = \_ -> pure []
            , neBallotsForPeriod = \_ _ _ -> pure []
            , neVoters = \_ p _ -> if p == Just 0 then pure [IT.PeriodVoter mockDelegate 0] else pure []
            , neBlockCount = pure 1599841
            , neGetProposal = \_ -> pure mockProposal
            , neGetProposals = \_ -> pure [mockProposal]
            , neProposalOperation = \_ p _ -> if p == Just 0 then pure [mockOperation] else pure []
            , neProposalOperationsForPeriod = \p pg _ -> if (p == Just 0 && pg == Just 0) then pure [mockProposalOperation] else pure []
            , neProtocols = \_ _ -> pure [mockProtocol]
            , neEpoch = \i ->  pure $ IT.EpochInfo i [mockPeriodInfo]
            , neEpochs = pure [IT.EpochInfo 0 [mockPeriodInfo]]
            }
      (periodInfo, _, _) <- runWithMocksReturnDiscourse_ indxEp $ getPeriodInfo (Just 0)
      (WT._piWinner periodInfo) `shouldBe` (Nothing :: (Maybe WT.Proposal))

      (votes, _, _) <- runWithMocksReturnDiscourse_ indxEp $ getSpecificProposalVotes (AT.encodeHash "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i")
      let expectedVote = WT.ProposalVote
            { _pvId = AT.Id 100
            , _pvProposal = mockProtocolHash
            , _pvProposalTitle = Just "(PsFLorena)"
            , _pvAuthor = WT.Baker
                { _bkPkh = AT.encodeHash "tz1fJHFn6sWEd3NnBPngACuw2dggTv6nQZ7g"
                , _bkRolls = AT.Rolls 0
                , _bkName = "Baking Team"
                , _bkLogoUrl = Just "https://services.tzkt.io/v1/avatars/tz1fJHFn6sWEd3NnBPngACuw2dggTv6nQZ7g"
                , _bkProfileUrl = Just "https://tzkt.io/tz1fJHFn6sWEd3NnBPngACuw2dggTv6nQZ7g/operations/voting"
                }
            , _pvOperation = AT.encodeHash "BKxssbv2J7oqhHU3cZDfbzXPXvpAnz3rgSCxG2jT2sdeMfAKbj2"
            , _pvTimestamp = dummyTime
            }
      votes `shouldBe` [expectedVote]

    it "check non-voter filtering only consider delegate address" $ do
      -- We return two delegates from the `neVoters` endpoint and
      -- then return one ballot from the `neBallotsForPeriod` endpoint.
      -- and check that the non-voters endpoint does not include the delegate
      -- corresponding to the ballot.
      let dummyTime = parseUTCTime "2021-06-04T11:27:03Z"
      let mockProtocolHash = AT.encodeHash "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
      let mockPeriodInfo = IT.PeriodInfo
            { kind = IT.Proposing
            , firstLevel = 400
            , lastLevel = 4096
            , startTime = dummyTime
            , endTime = dummyTime
            , status = Nothing
            , epoch = 0
            , yayBallots = Nothing
            , yayRolls = Nothing
            , nayBallots    = Nothing
            , nayRolls      = Nothing
            , passBallots   = Nothing
            , passRolls     = Nothing
            , ballotsQuorum = Nothing
            , supermajority = Nothing
            , totalBakers   = Nothing
            , totalRolls    = Nothing
            , index = 0
            }
      let mockProtocol = IT.Protocol
            { hash = mockProtocolHash
            , firstLevel = 200
            , lastLevel = Nothing
            , constants = IT.Constants
                { proposalQuorum = Just 5
                , blocksPerCycle = Just 4096
                , blocksPerVoting = Just 32768
                }
            }
      let mockDelegate = IT.Delegate
            { address = AT.encodeHash "tz1fJHFn6sWEd3NnBPngACuw2dggTv6nQZ7g" -- This address has to be from the "bakers.json" file in the resources directory.
            , alias = Nothing
            , logoUrl = Nothing
            }

      let mockDelegate2 = IT.Delegate
            { address = AT.encodeHash "tz1fUyqYH7H4pRHN1roWY17giXhy1RvxjgUV" -- This address has to be from the "bakers.json" file in the resources directory.
            , alias = Just "somealias"
            , logoUrl = Just "logo"
            }

      let mockBallot = IT.Ballot
            { delegate = IT.Delegate (IT.address mockDelegate2) Nothing Nothing
            , vote = AT.Yay
            , rolls = fromInteger 10
            , id = 100
            , hash = AT.encodeHash "onw6yGKFPucybjz5ysTRoohdnZMt9UFkTo6RdUFe7vcMp5aGv4s"
            , timestamp = dummyTime
            , proposal = IT.SimpleProposal Nothing $ AT.encodeHash "BKxssbv2J7oqhHU3cZDfbzXPXvpAnz3rgSCxG2jT2sdeMfAKbj2"
            }

      let mockProposalOperation = IT.Operation
            { timestamp = dummyTime
            , level = 250
            , hash = AT.encodeHash "BKxssbv2J7oqhHU3cZDfbzXPXvpAnz3rgSCxG2jT2sdeMfAKbj2"
            , proposal = Nothing
            , delegate = mockDelegate
            }

      let mockProposal = IT.Proposal
                { epoch = 1
                , rolls = 0
                , hash = AT.encodeHash "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
                , firstPeriod =  0
                , lastPeriod = 0
                , initiator = mockDelegate
                , status = IT.Active
                }
      let indxEp = IndexerEndpointsIO
            { neLevel = \_ _ -> do
                pure $ IT.Block
                  { level = 600
                  , timestamp = parseUTCTime "2021-06-04T11:27:03Z"
                  }
            , neHead = pure $ IT.Head
                { hash = AT.encodeHash "BKxssbv2J7oqhHU3cZDfbzXPXvpAnz3rgSCxG2jT2sdeMfAKbj2"
                , level = 600
                , protocol = AT.encodeHash "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
                , timestamp = parseUTCTime "2021-06-04T11:27:03Z"
                }
            , neBallotsForProposal = \_ -> pure []
            , neBallotsForPeriod = \period mPage _ -> if period == Just 0 && mPage == Just 0 then pure [mockBallot] else pure []
            , neVoters = \_ p _ -> if p == Just 0 then pure [IT.PeriodVoter mockDelegate 1, IT.PeriodVoter mockDelegate2 1] else pure []
            , neBlockCount = pure 1599841
            , neGetProposal = \_ -> pure mockProposal
            , neGetProposals = \_ -> pure [mockProposal]
            , neProposalOperation = \_ p _ -> if p == Just 0 then pure [mockProposalOperation] else pure []
            , neProposalOperationsForPeriod = \_ _ _ -> pure []
            , neProtocols = \_ _ -> pure [mockProtocol]
            , neEpoch = \i ->  pure $ IT.EpochInfo i [mockPeriodInfo]
            , neEpochs = pure [IT.EpochInfo 0 [mockPeriodInfo]]
            }
      (nonVoters, _, _) <- runWithMocksReturnDiscourse_ indxEp $ getNonVoters (fromInteger 0)
      (WT._bkPkh <$> nonVoters) `shouldBe` [IT.address mockDelegate]
