module Agora.Indexer.JsonSpec
      ( spec
      ) where

import Data.Aeson (FromJSON, eitherDecode')
import qualified Data.ByteString.Lazy as BS
import Test.Hspec (Spec, describe, expectationFailure, it, shouldBe)

import Agora.Indexer.Types as IT
import Agora.Types as AT
import Agora.Arbitrary ()
import Agora.Util

spec :: Spec
spec =
  describe "JSON decoding" $ do
    it "can decode PeriodInfo" $
      testDecoding "resources/indexer/PeriodInfo.json" IT.PeriodInfo
        { kind = IT.Proposing
        , firstLevel = Level 1
        , lastLevel = Level 32768
        , startTime = parseUTCTime "2018-06-30T17:39:57Z"
        , endTime = parseUTCTime "2018-07-24T22:19:27Z"
        , status = Just PNoProposals
        , epoch = Id 0
        , yayBallots = Nothing
        , yayRolls = Nothing
        , nayBallots = Nothing
        , nayRolls = Nothing
        , passBallots = Nothing
        , passRolls = Nothing
        , ballotsQuorum = Nothing
        , supermajority = Nothing
        , totalBakers = Just 8
        , totalRolls = Just (Rolls 15280)
        , index = Id 0
        }

    it "can decode Block" $
      testDecoding "resources/indexer/Block.json" Block
        { level = 5000
        , timestamp = parseUTCTime "2018-07-04T05:20:12Z"
        }

    it "can decode Head" $
      testDecoding "resources/indexer/Head.json" Head
        { level = 1400832
        , hash = encodeHash "BLTv2V9TjbhxPTeptQeasfGRymKDnQ8SWKzxsanPJyrZK4tYTgQ"
        , protocol = encodeHash "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA"
        , timestamp = parseUTCTime "2021-03-26T01:18:52Z"
        }

    it "can decode Ballot" $
      testDecoding "resources/indexer/Ballot.json" Ballot
        { delegate = Delegate
            { address = encodeHash "tz1NkFRjmkqqcGkAhqe78fdgemDNKXvL7Bod"
            , alias = Nothing
            , logoUrl = Nothing
            }
        , vote = Yay
        , rolls = 2
        , id = 39479474
        , hash = encodeHash "oni6PHJqWnMLCVQnhRf8NesH7m3UBSL7dFKA5aEH63tyJ3WCDG2"
        , timestamp = parseUTCTime "2021-01-21T05:03:03Z"
        , proposal = SimpleProposal
            { alias = Nothing
            , hash = encodeHash "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
            }
        }

    it "can decode PeriodVoter" $
      testDecoding "resources/indexer/PeriodVoter.json" PeriodVoter
        { delegate = Delegate
            { address = encodeHash "tz1W5VkdB5s7ENMESVBtwyt9kyvLqPcUczRT"
            , alias = Nothing
            , logoUrl = Nothing
            }
        , rolls = 86
        }
    it "can decode Proposal" $
      testDecoding "resources/indexer/Proposal.json" Proposal
        { epoch = 26
        , rolls = 13530
        , hash = encodeHash "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
        , firstPeriod = Id 42
        , lastPeriod = Id 43
        , initiator = Delegate
            { address = encodeHash "tz1S5WxdZR5f9NzsPXhr7L9L1vrEb5spZFur"
            , alias = Nothing
            , logoUrl = Nothing
            }
        , status = Active
        }
    it "can decode Operation" $
      testDecoding "resources/indexer/Operation.json" Operation
        { timestamp = parseUTCTime "2019-02-28T13:05:10Z"
        , level = Level 332624
        , hash = encodeHash "onydFJLWdGhfKNBfbnSLmqDu93j9NRimkbQm9WqLWYG8eyZUyTF"
        , proposal = Just $ SimpleProposal
            { alias = Nothing
            , hash = encodeHash "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
            }
        , delegate = Delegate
            { address = encodeHash "tz1fNdh4YftsUasbB1BWBpqDmr4sFZaPNZVL"
            , alias = Nothing
            , logoUrl = Nothing
            }
        }
    it "can decode ProposalOperation" $
      testDecoding "resources/indexer/Operation.json" ProposalOperation
        { id = 7602798
        , rolls = 11
        , timestamp = parseUTCTime "2019-02-28T13:05:10Z"
        , hash = encodeHash "onydFJLWdGhfKNBfbnSLmqDu93j9NRimkbQm9WqLWYG8eyZUyTF"
        , proposal = SimpleProposal
            { alias = Nothing
            , hash = encodeHash "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
            }
        , delegate = Delegate
            { address = encodeHash "tz1fNdh4YftsUasbB1BWBpqDmr4sFZaPNZVL"
            , alias = Nothing
            , logoUrl = Nothing
            }
        }

    it "can decode EpochInfo" $
      testDecoding "resources/indexer/EpochInfo.json" EpochInfo
        { index = Id 0
        , periods = []
        }

testDecoding
  :: (Show a, Eq a, FromJSON a)
  => FilePath -> a -> IO ()
testDecoding path expected = do
  content <- BS.readFile path
  case eitherDecode' content of
    Left err  -> expectationFailure err
    Right res -> res `shouldBe` expected
