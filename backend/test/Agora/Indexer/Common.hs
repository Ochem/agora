module Agora.Indexer.Common
  ( afp
  , checkExpectationFileOrWrite
  , proposalHashes
  , runWithMocks
  , runWithMocksReturnDiscourse
  , runWithMocksReturnDiscourse_
  ) where

import Control.Monad.Reader (withReaderT)
import Data.Aeson (FromJSON, ToJSON, decodeFileStrict, eitherDecodeFileStrict)
import Data.Aeson.Encode.Pretty (defConfig, encodePretty')
import qualified Data.ByteString.Lazy as BSL
import qualified Data.List.NonEmpty as NE
import qualified Data.Map as Map
import Loot.Log
import Loot.Log.Internal (NameSelector(..))
import Monad.Capabilities (CapImpl(..), CapsT, addCap, emptyCaps)
import System.Directory (createDirectoryIfMissing, doesFileExist)
import System.FilePath (takeDirectory, (<.>), (</>))
import Test.Hspec (shouldBe)
import UnliftIO (MonadUnliftIO)

import Agora.BakerFetch.Api
import Agora.Config.Definition
import Agora.Discourse.API
import Agora.Discourse.Client
import Agora.Discourse.Types
import Agora.Indexer.Capability
import Agora.Metrics.Capability
import Agora.State.Capability
import Agora.Mode
import Agora.TestMode (invalidWaiUrls, testingConfig)
import qualified Agora.Types as AT
import Agora.Util

runWithMocks
  :: CapsT AgoraCaps AT.Base a
  -> IO a
runWithMocks action = do
  (a, _, _) <- runWithMocksReturnDiscourse action
  pure a

-- This function runs an action in AgoraCaps capability in the Agora environment
-- using mocks. It returns the result of the action, along with the list of discourse
-- topics and posts created.
--
-- The recordingMockIndexerEndpointsIO can run a mock indexed using previously recorded
-- data, if it is available, or else it will invoke the actions in the `IndexerEndpointsIO`
-- argument. The recorded data can be updated by deleting existing data, and running this test
-- with a real indexer, possibly running locally. It can be obtained as follows.
--
-- The refresh inteval needs to be around 10 seconds because the fetching data from real indexer is
-- slower then fetching it from files.
--
--    bUrl <- parseBaseUrl "http://localhost:5000/v1"
--    env <- getClientEnv bUrl
--    realIndexer <- getIndexerReal env
--    ...
--    ...
--    withIndexer (recordingMockIndexerEndpointsIO realIndexer) 10 $ do
--      waitFor 25
--      action
--

runWithMocksReturnDiscourse
  :: CapsT AgoraCaps AT.Base a
  -> IO (a, [Topic], [Post])
runWithMocksReturnDiscourse action = runWithMocksReturnDiscourse_ (recordingMockIndexerEndpointsIO (error "unavailable")) action

runWithMocksReturnDiscourse_
  :: IndexerEndpointsIO
  -> CapsT AgoraCaps AT.Base a
  -> IO (a, [Topic], [Post])
runWithMocksReturnDiscourse_ idxEp action = do
 (discourseIOMock, topicsVar, postsVar) <- mockDiscourseIO
 discourseIOTVar <- newTVarIO discourseIOMock
 r <- usingReaderT emptyCaps $

     withStateCap $
     withReaderT (addCap mockLogging) $
     withReaderT (addCap mockConfig) $
     withReaderT (addCap mockMetrics) $
     withReaderT (addCap (mockDiscourseClientRaw discourseIOTVar)) $
     withDiscourseClient $
     withReaderT (addCap mockBakerFetch) $

     withIndexer idxEp 0 $ do
       waitFor 6
        --This should be enough time for the indexer to atleast refresh a couple of times
        --So that the discourse topic creation is triggered by the first run, and updates
        --picked up in the second.
       action

 createdPosts <- atomically $ readTVar postsVar
 createdTopics <- atomically $ readTVar topicsVar
 pure (r, Map.elems createdTopics, Map.elems createdPosts)

  where

    mockDiscourseClientRaw :: MonadIO m => TVar DiscourseEndpointsIO -> CapImpl DiscourseClientRaw '[] m
    mockDiscourseClientRaw discourceIOTVar = CapImpl $ DiscourseClientRaw
      { _getDiscourseClientRaw = atomically $ readTVar discourceIOTVar
      , _putDiscourseClientRaw = \dio -> atomically $ writeTVar discourceIOTVar dio
      }

    mockLogging :: MonadIO m => CapImpl Logging '[] m
    mockLogging  = CapImpl $ Logging
      { _log  = \_ -> pure () -- putTextLn $ decodeUtf8 $ fmtMessageFlat e (uncomment to enable logs during tests)
      , _logName  = pure $ GivenName "Dummy"
      }

    mockMetrics :: Monad m => CapImpl MetricsCap '[] m
    mockMetrics  = CapImpl $ MetricsCap
      { _sendMetrics  = \_ -> pure ()
      }

    mockBakerFetch :: MonadUnliftIO m => CapImpl BakerFetchCap BakerFetchDeps m
    mockBakerFetch  = bakerFetchCap $ BakerFetchEndpointsIO
      { bfGetBakers = fromMaybe (error "Error decoding bakers.json") <$> decodeFileStrict "resources/indexer/bakers.json"
      }

    mockConfig :: Monad m => CapImpl AgoraConfigCap '[] m
    mockConfig  = newConfig (testingConfig invalidWaiUrls)

mockDiscourseIO :: IO (DiscourseEndpointsIO, TVar (Map.Map AT.DiscourseTopicId Topic), TVar (Map.Map AT.DiscoursePostId Post))
mockDiscourseIO = do
  topics <- newTVarIO $ Map.fromList [(existingTopicId, existingTopic)]
  posts <- newTVarIO mempty
  d <- mockDiscourseIO' topics posts
  pure (d, topics, posts)

  where
    existingTopicId = fromInteger 1000
    existingTopic = MkTopic existingTopicId (Title "(PsDELPH1K) Manually made") (NE.fromList [existingPost])
    existingPost = Post (fromInteger 1010) existingTopicId "Some manually made post"

    mockDiscourseIO'
      :: TVar (Map.Map AT.DiscourseTopicId Topic)
      -> TVar (Map.Map AT.DiscoursePostId Post)
      -> IO DiscourseEndpointsIO
    mockDiscourseIO' topics posts = do
      pure DiscourseEndpointsIO
        { dePostTopicIO  = \_ _ ct -> do
            newTopic <- mkNewTopic ct
            pure newTopic
        , deGetCategoryTopicsIO = \cid _ ->
            if cid == 11
              then getTopics
              else error "Unexpected category id"
        , deGetCategoriesIO = pure $ CategoryList [Category 10 "SomethingElse", Category 11 "Proposals"]
        , deGetPostIO = \postId -> do
            mPosts <- atomically $ readTVar posts
            pure $ fromMaybe (error $ "Post not found for" <> (show postId)) $ Map.lookup postId mPosts
        , deGetTopicIO = \topicId -> do
            mTopics <- atomically $ readTVar topics
            pure $ fromMaybe (error $ "Topic not found" <> (show topicId)) $ Map.lookup topicId mTopics
        }
      where
        getTopics :: IO CategoryTopics
        getTopics = do
          topics_ <- Map.elems <$> (atomically $ readTVar topics)
          pure $ CategoryTopics 100 $ (\t -> TopicHead (tId t) (tTitle t)) <$> topics_

        mkNewTopic :: CreateTopic -> IO CreatedTopic
        mkNewTopic CreateTopic {..} = do
          newTopicId <- getNewTopicId
          newPostId <- getNewPostId
          let newPost = Post newPostId newTopicId (unRawBody ctRaw)
          let newTopic = MkTopic newTopicId ctTitle (NE.fromList [newPost])
          atomically $ modifyTVar' posts (Map.insert newPostId newPost)
          atomically $ modifyTVar' topics (Map.insert newTopicId newTopic)
          pure $ CreatedTopic newPostId newTopicId

        getNewTopicId :: IO AT.DiscourseTopicId
        getNewTopicId = do
          keys_ <- Map.keys <$> (atomically $ readTVar topics)
          let topicId = if (null keys_) then (fromInteger 0) else (maximum keys_ + 1)
          pure topicId

        getNewPostId :: IO AT.DiscoursePostId
        getNewPostId = do
          keys_ <- Map.keys <$> (atomically $ readTVar posts)
          pure $ if (null keys_) then (fromInteger 0) else (maximum keys_ + 1)

class ToFilePart a where
  toFilePart :: a -> FilePath

instance ToFilePart Text where
  toFilePart = toString

instance ToFilePart (AT.Id a) where
  toFilePart (AT.Id a) = show a

instance ToFilePart (AT.Hash a) where
  toFilePart = toString . AT.hashToText

instance {-# OVERLAPS #-} (Num a, Show a) => ToFilePart a where
  toFilePart = show

instance {-# OVERLAPS #-} (ToFilePart a) => ToFilePart (Maybe a) where
  toFilePart Nothing  = ""
  toFilePart (Just a) = toFilePart a

-- This is a mock for the Indexer that can relay requests to a
-- real indexer, and record the responses obtained from the real indexer to a file.
-- This captures the indexer behavior so that we can later test our code against the
-- recorded indexer behavior. When running tests against the recorded data, we just provide
-- and `undefined` or `error` value for the `realIndexer` argument.
recordingMockIndexerEndpointsIO :: IndexerEndpointsIO ->  IndexerEndpointsIO
recordingMockIndexerEndpointsIO realIndexer = IndexerEndpointsIO
  { neLevel = \(lvl@(AT.Level l)) s ->
      readRecorded ("levels" </> (afp "" l)) (neLevel realIndexer lvl s)
  , neHead = readRecorded "head" (neHead realIndexer)
  , neBallotsForProposal = \pHash ->
        readRecorded ("ballots-for-proposal" </> (afp "" pHash))
          (neBallotsForProposal realIndexer pHash)
  , neBallotsForPeriod = \pid off lim ->
      readRecorded ("ballots-for-period" </> (afp "" pid) `afp` off `afp` lim)
          (neBallotsForPeriod realIndexer pid off lim)
  , neVoters = \pid off lim ->
      readRecorded ("voters-for-period" </> ((afp "" pid) `afp` off `afp` lim))
        (neVoters realIndexer pid off lim)
  , neBlockCount = readRecorded "block-count" (neBlockCount realIndexer)
  , neGetProposal = \phash ->
      readRecorded ("proposals" </> (afp "" phash)) (neGetProposal realIndexer phash)
  , neGetProposals = \epochId -> do
      readRecorded ("proposals-by-epoch" </> (afp "" epochId)) (neGetProposals realIndexer epochId)
  , neProposalOperation = \pid off lim ->
      readRecorded ("proposals-operation-for-proposal" </>
        (afp "" pid) `afp` off `afp` lim) (neProposalOperation realIndexer pid off lim)
  , neProposalOperationsForPeriod = \pid off lim ->
      readRecorded ("proposals-operation-for-proposal" </>
        ((afp "" pid) `afp` off `afp` lim)) (neProposalOperationsForPeriod realIndexer pid off lim)
  , neProtocols = \off lim ->
      readRecorded ("protocols" </>  (afp "protocols" off `afp` lim)) (neProtocols realIndexer off lim)
  , neEpoch = \epochId ->  readRecorded ("epoch" </> (afp "" epochId)) (neEpoch realIndexer epochId)
  , neEpochs = readRecorded "epochs" (neEpochs realIndexer)
  }
  where

    testFilesDir :: FilePath
    testFilesDir = "resources/indexer/test-data"

    readRecorded :: (ToJSON a, FromJSON a) => FilePath -> IO a -> IO a
    readRecorded fpin real =
      doesFileExist fp >>= \case
        True -> do
          eitherDecodeFileStrict fp >>= \case
            Right a  -> pure a
            Left err -> error (toText err)
        False -> do
          a <- real
          encodeFileCreatingDir fp a
          pure a
      where
        fp = testFilesDir </> fpin <.> "json"

afp :: ToFilePart a => FilePath -> a -> FilePath
afp "" a     = toFilePart a
afp prefix a = prefix <> "-" <> (toFilePart a)

encodeFileCreatingDir :: ToJSON a => FilePath -> a -> IO ()
encodeFileCreatingDir fp a = do
  createDirectoryIfMissing True (takeDirectory fp)
  BSL.writeFile fp (encodePretty' defConfig a)

-- To make this function update the expectation, do the following at the
-- begining of the function.
--    encodeFileCreatingDir expectationFile real
checkExpectationFileOrWrite :: (Eq a, Show a, ToJSON a, FromJSON a) => FilePath -> a -> IO ()
checkExpectationFileOrWrite expectationFile real = do
  expected <- fromMaybe (error "error decoding expectation") <$> decodeFileStrict expectationFile
  real `shouldBe` expected

proposalHashes :: [AT.ProposalHash]
proposalHashes = AT.encodeHash <$>
      [ "Psd1ynUBhMZAeajwcZJAeq5NrxorM6UCU4GJqxZ7Bx2e9vUWB6z"
      , "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
      , "PtdRxBHvc91c2ea2evV6wkoqnzW7TadTg9aqS9jAn2GbcPGtumD"
      , "PsBABY5nk4JhdEv1N1pZbt6m6ccB9BfNqa23iKZcHBh23jmRS9f"
      , "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU"
      , "PtCarthavAMoXqbjBPVgDCRd5LgT7qqKWUPXnYii3xCaHRBMfHH"
      , "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
      , "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
      , "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
      , "PsFLorBArSaXjuy9oP76Qv1v2FRYnUs7TFtteK5GkRBC24JvbdE"
      , "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
      , "Psd1ynUBhMZAeajwcZJAeq5NrxorM6UCU4GJqxZ7Bx2e9vUWB6z"
      , "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
      , "PtdRxBHvc91c2ea2evV6wkoqnzW7TadTg9aqS9jAn2GbcPGtumD"
      , "PsBABY5nk4JhdEv1N1pZbt6m6ccB9BfNqa23iKZcHBh23jmRS9f"
      , "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU"
      , "PtCarthavAMoXqbjBPVgDCRd5LgT7qqKWUPXnYii3xCaHRBMfHH"
      , "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
      , "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
      , "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
      , "PsFLorBArSaXjuy9oP76Qv1v2FRYnUs7TFtteK5GkRBC24JvbdE"
      , "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
      , "Psd1ynUBhMZAeajwcZJAeq5NrxorM6UCU4GJqxZ7Bx2e9vUWB6z"
      , "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
      , "PtdRxBHvc91c2ea2evV6wkoqnzW7TadTg9aqS9jAn2GbcPGtumD"
      , "PsBABY5nk4JhdEv1N1pZbt6m6ccB9BfNqa23iKZcHBh23jmRS9f"
      , "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU"
      , "PtCarthavAMoXqbjBPVgDCRd5LgT7qqKWUPXnYii3xCaHRBMfHH"
      , "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
      , "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
      , "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
      , "PsFLorBArSaXjuy9oP76Qv1v2FRYnUs7TFtteK5GkRBC24JvbdE"
      , "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"]
