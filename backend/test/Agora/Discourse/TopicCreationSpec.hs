module Agora.Discourse.TopicCreationSpec
      ( spec
      ) where

import Data.Map as Map
import Network.HTTP.Types.Status (status422)
import Network.HTTP.Types.Version (http20)
import Servant.Client.Core.Response (Response, ResponseF(..))
import Servant.Client.Streaming (ClientError(..))
import Test.Hspec (Spec, describe, it, shouldReturn)
import qualified UnliftIO as UIO

import Agora.Discourse.API
import Agora.Discourse.Client
import Agora.Discourse.Types
import Agora.Indexer.Common
import Agora.Types as AT
import Agora.Util

newtype TestError = TestError Text
  deriving Show

instance Exception TestError

spec :: Spec
spec =
  describe "Topic creation behavior" $
    it "does not retry proposal creation if it has failed irrecoverably in the past" $ do
      ccTrackerTvar <- liftIO $ newTVarIO mempty
      runWithMocks $ do
        putDiscourseClientRaw (mockDiscourseRaw ccTrackerTvar)
        UIO.withAsync (do
          -- We call this three times, but since a irrecoverable error
          -- is thrown, only the first call should be tried.
          postProposalStubAsync (AT.Hash "dummyHash")
          postProposalStubAsync (AT.Hash "dummyHash")
          postProposalStubAsync (AT.Hash "dummyHash")
          ) $ \_ -> waitFor 2
      (Map.lookup "(dummyHash)" <$> (atomically $ readTVar ccTrackerTvar)) `shouldReturn` (Just 1)
  where
    testError = "Irrecoverable proposal creation error"

    mockDiscourseRaw :: TVar (Map Text Int) -> DiscourseEndpointsIO
    mockDiscourseRaw ccTracker = DiscourseEndpointsIO
      { dePostTopicIO = \_ _ ct -> do
          let title = unTitle $ ctTitle ct
          liftIO $ atomically $ UIO.modifyTVar ccTracker (\m -> case Map.lookup title m of
            Just x -> Map.insert title (x+1) m
            Nothing -> Map.insert title 1 m
            )
          UIO.throwIO dummyError
      , deGetCategoryTopicsIO = \_ _ -> pure $ CategoryTopics 0 []
      , deGetCategoriesIO = pure $ CategoryList []
      , deGetTopicIO = \_ -> UIO.throwIO dummyError
      , deGetPostIO = \_ -> pure $ Post 0 0 ""
      }

    dummyError :: ClientError
    dummyError = DecodeFailure testError dummyResponse

    dummyResponse :: Response
    dummyResponse = Response status422 mempty http20 mempty
