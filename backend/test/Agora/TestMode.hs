{-# LANGUAGE OverloadedLabels #-}

{-|
Defines the monadic stack and parameters for testing monadic Agora code.
-}
module Agora.TestMode
  ( WaiUrls
  , invalidWaiUrls
  , emptyDiscourseEndpoints
  , testDiscourseCategory
  , testingConfig
  ) where

import Prelude hiding (ByteString)

import Lens.Micro.Platform ((?~))
import Loot.Log (basicConfig)

import Servant.Client (BaseUrl(..), Scheme(..))
import Servant.Client.Generic (AsClientT)

import Agora.BakerFetch.Types
import Agora.Config
import Agora.Discourse
import Agora.Types

data WaiUrls = WaiUrls
  { discourseUrl  :: BaseUrl
  , bakerFetchUrl :: BaseUrl
  , indexerUrl    :: BaseUrl
  } deriving Show

invalidWaiUrls :: WaiUrls
invalidWaiUrls = WaiUrls
  { discourseUrl = BaseUrl Https "tezos.discourse.invalid" 443 ""
  , bakerFetchUrl = BaseUrl Https "tezos.baker.invalid" 443 ""
  , indexerUrl = BaseUrl Https "indexer.invalid" 443 ""
  }

emptyDiscourseEndpoints :: DiscourseEndpoints (AsClientT m)
emptyDiscourseEndpoints = DiscourseEndpoints
  { dePostTopic = error "dePostTopic isn't supposed to be called"
  , deGetCategoryTopics = error "deGetCategoryTopics isn't supposed to be called"
  , deGetCategories = error "deGetCategories isn't supposed to be called"
  , deGetTopic = error "deGetTopic isn't supposed to be called"
  , deGetPost = error "deGetPost isn't supposed to be called"
  }

testDiscourseCategory :: Text
testDiscourseCategory = "Proposals"

-- | Configuration which is used in tests. Accepts a `ConnString`
-- which is determined at runtime.
testingConfig :: WaiUrls -> AgoraConfigRec
testingConfig urls = finaliseDeferredUnsafe $ mempty
  & option #logging ?~ basicConfig
  & option #bakerfetch_url ?~ (bakerFetchUrl urls)
  & option #indexer_url ?~ (indexerUrl urls)
  & sub #discourse . option #host ?~ (discourseUrl urls)
  & sub #discourse . option #category ?~ testDiscourseCategory
  & option #predefined_bakers  ?~
      [ BakerInfo "Foundation Baker 1" (encodeHash "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9") Nothing Nothing
      , BakerInfo "Foundation Baker 2" (encodeHash "tz3bvNMQ95vfAYtG8193ymshqjSvmxiCUuR5") Nothing Nothing
      ]
