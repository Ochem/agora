{-# LANGUAGE OverloadedLabels #-}

{-|
Definitions of monadic contstraints commonly used by the workers
and handlers.
-}
module Agora.Mode
       ( AgoraWorkMode
       , AgoraCaps
       , runAgoraReal
       , setEncoding
       ) where

import Loot.Log (Logging, MonadLogging, NameSelector(..), withLogging)
import Monad.Capabilities (CapsT, emptyCaps)
import System.IO (hSetEncoding, utf8)
import UnliftIO (MonadUnliftIO)

import Agora.BakerFetch.Api
import Agora.Config
import Agora.Discourse
import Agora.Indexer.Capability
import Agora.Metrics.Capability
import Agora.State.Capability
import qualified Agora.Types as AT

-- | Common set of constraints for Agora business logic.
type AgoraWorkMode m =
  ( MonadIO m
  , MonadUnliftIO m
  , MonadStateCap m
  , MonadLogging m
  , MonadConfig AgoraConfig m
  , MonadMetricsCap m
  , MonadBakerFetchCap m
  , MonadIndexerCap m
  )

-- | List of capabilities required for `AgoraWorkMode`
type AgoraCaps = '[
    IndexerCap
  , BakerFetchCap
  , DiscourseClient
  , DiscourseClientRaw
  , MetricsCap
  , AgoraConfigCap
  , Logging
  , StateCap
  ]

setEncoding :: IO ()
setEncoding = do
  hSetEncoding stdout utf8
  hSetEncoding stderr utf8

-- | Runs an action which requires an @AgoraWorkMode@ in @CapsT@ over @IO@.
runAgoraReal
  :: AgoraConfigRec
  -> CapsT AgoraCaps AT.Base a
  -> IO a
runAgoraReal config action = do
  liftIO setEncoding
  usingReaderT emptyCaps
    . withStateCap
    . withLogging (config ^. option #logging) CallstackName
    . withConfig config
    . withMetricsCap
    . withDiscourseClientRaw
    . withDiscourseClient $ do
        bakerfetchUrl <- fromAgoraConfig $ option #bakerfetch_url
        indexerUrl <- fromAgoraConfig $ option #indexer_url
        env <- liftIO $ getClientEnv indexerUrl
        bfEnv <- liftIO $ getClientEnv bakerfetchUrl
        bakerFetchEps <- getBakerFetchReal bfEnv
        indexerEps <- getIndexerReal env
        withBakerFetch bakerFetchEps $
          withIndexer indexerEps 300 action
