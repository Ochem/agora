module Agora.State.Capability
  ( AgoraState(..)
  , StateCap
  , emptyAgoraState
  , modifyAgoraState
  , MonadStateCap(..)
  , withStateCap
  ) where

import Control.Monad.Reader (withReaderT)
import Monad.Capabilities (CapImpl(..), CapsT, HasCap, HasNoCap, addCap, makeCap)
import UnliftIO (MonadUnliftIO)

import qualified Agora.Types as AT

type ProposalCreationError = Text

data AgoraState = AgoraState
  { asProposalCreationFailures :: Map AT.ProposalHash ProposalCreationError
  }

emptyAgoraState :: AgoraState
emptyAgoraState = AgoraState mempty

-- | A capability to add some stateful data to Agora. Right now only
-- used to track discourse topic creation permenant errors, and skip re-trying
-- creation.
data StateCap m = StateCap
  { _setState :: AgoraState -> m ()
  , _getState :: m AgoraState
  }

makeCap ''StateCap

withStateCap
  :: forall m caps a.
  ( MonadUnliftIO m
  , HasNoCap StateCap caps
  )
  => CapsT (StateCap ': caps) m a
  -> CapsT caps m a
withStateCap action = do
  stateTVar <- liftIO $ newTVarIO emptyAgoraState
  withReaderT (addCap $ stateCap stateTVar) action

stateCap :: MonadUnliftIO m => TVar AgoraState -> CapImpl StateCap '[] m
stateCap tvar = CapImpl $ StateCap
  { _setState = \s -> liftIO $ atomically $ writeTVar tvar s
  , _getState = liftIO $ atomically $ readTVar tvar
  }

modifyAgoraState :: (Monad m, HasCap StateCap caps) => (AgoraState -> AgoraState) -> CapsT caps m ()
modifyAgoraState fn = getState >>= (pure . fn) >>= setState
