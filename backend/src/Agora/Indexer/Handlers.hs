{-# LANGUAGE DuplicateRecordFields #-}

{-|

Description: This module mostly handles the implementation of the Agora
endpoints using the data fetched from the Indexer via the Indexer capability.

|-}

module Agora.Indexer.Handlers
  ( getBallots
  , getNonVoters
  , getPeriodInfo
  , getProposal
  , getProposalVotes
  , getProposals
  , getSpecificProposalVotes
  ) where

import Data.List ((!!))
import qualified Data.List as DL
import qualified Data.Map as Map
import Data.Set as S
import Fmt (build, fmt)

import Agora.Config.Definition
import Agora.Indexer.Capability
import Agora.Indexer.Types
import Agora.Mode
import qualified Agora.Types as AT
import qualified Agora.Web.Types as AT

getPeriodInfo
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => Maybe AT.PeriodId
  -> m AT.PeriodInfo
getPeriodInfo mpId = do
  periods <- idxPeriods
  let currentPeriod = maximum (periodIndex <$> periods)
  let pId = case mpId of
        Just p  -> p
        Nothing -> currentPeriod
  let pIdInt = fromIntegral pId
  let periodInfos = getPeriodItemInfo <$> periods
  case (periods !! pIdInt, periodInfos !! pIdInt) of
    (PeriodInfo {..}, AT.PeriodItemInfo {..}) -> do
      oneCycle <- (fromIntegral . fromMaybe 4096) <$> (blocksPerCycle <$> (getConstantsForLevel firstLevel))
      chainHead <- idxHead
      discourseHost <- askDiscourseHost
      let hl = headLevel chainHead

      -- This is the last available level that is also
      -- in this period.
      let currentPeriodLevel =
            if hl > lastLevel then lastLevel else hl

      let period :: AT.Period = AT.Period
            pId
            firstLevel
            currentPeriodLevel
            lastLevel
            _piiStartTime
            _piiEndTime
            (fromIntegral $ (currentPeriodLevel - firstLevel + 1) `div` oneCycle)
            (fromIntegral $ (lastLevel - firstLevel + 1) `div` oneCycle)
      case kind of
        Proposing -> do
          mProposal <- getWonProposalForPeriod_ pId currentPeriod epoch >>= traverse convertProposal
          votesStats <- getProposalVotesStatForProposalPeriod pId
          pure $ AT.ProposalInfo period (fromIntegral $ length periods)
            periodInfos
            votesStats
            mProposal
            discourseHost

        Exploration -> do
          proposal <- getWonProposalForPeriod (pId - 1) currentPeriod epoch >>= convertProposal
          votesStats <- getVotesStat pId
          ballots <- fromMaybe (error "Didn't find ballots for period") <$> getBallotStatsForPeriod epoch pId
          pure $ AT.ExplorationInfo period (fromIntegral $ length periods)
            periodInfos
            proposal
            ((== PSuccess) <$> status)
            votesStats
            ballots
            discourseHost

        Testing -> do
          proposal <- getWonProposalForPeriod (pId - 2) currentPeriod epoch >>= convertProposal
          pure $ AT.TestingInfo period (fromIntegral $ length periods)
            periodInfos
            proposal
            ((== PSuccess) <$> status)
            discourseHost

        Promotion -> do
          proposal <- getWonProposalForPeriod (pId - 3) currentPeriod epoch >>= convertProposal
          votesStats <- getVotesStat pId
          ballots <- fromMaybe (error "Didn't find ballots for period") <$> getBallotStatsForPeriod epoch pId
          pure $ AT.PromotionInfo period (fromIntegral $ length periods)
            periodInfos
            proposal
            ((== PSuccess) <$> status)
            votesStats
            ballots
            discourseHost

        Adoption -> do
          proposal <- getWonProposalForPeriod (pId - 4) currentPeriod epoch >>= convertProposal
          pure $ AT.AdoptionInfo period (fromIntegral $ length periods)
            periodInfos
            proposal
            discourseHost

getPeriodItemInfo
  :: HasCallStack
  => PeriodInfo
  -> AT.PeriodItemInfo
getPeriodItemInfo PeriodInfo {..} =
  AT.PeriodItemInfo startTime endTime (toAgoraPeriodType kind)

getBallots
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m [AT.Ballot]
getBallots periodId = do
  bakers <- idxVoters periodId
  (fmap (convertBallot bakers)) <$> (idxBallotsForPeriod periodId)

getSpecificProposalVotes
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.ProposalHash
  -> m [AT.ProposalVote]
getSpecificProposalVotes proposalHash_ = do
  periods <- idxPeriods
  let currentPeriod = maximum (periodIndex <$> periods)
  pr <- idxGetProposal proposalHash_
  let pId = firstPeriod pr
  proposalMeta <- idxGetProposalMeta $ proposalHash pr
  if currentPeriod > pId then do
    bakers <- idxVoters (pId + 1) -- Because voting happens in the periods following the period in which the proposal appeared.
    (fmap (convertProposalVote (Map.singleton (proposalHash pr) proposalMeta) bakers)) <$> (idxBallotsForProposal proposalHash_)
  else do
    bakers <- idxVoters pId
    proposalOps <- idxProposalOperationsForPeriod pId
    -- Filter out proposal operations that are not this proposal
    let filteredProposalOps = DL.filter ((== proposalHash_) . simpleProposalHash . proposalOperationProposal) proposalOps
    pure ((convertProposalVoteFromOperation (Map.singleton (proposalHash pr) proposalMeta) bakers) <$> filteredProposalOps)

getProposalMetas
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => Proposal
  -> m (AT.ProposalHash, Maybe ProposalMeta)
getProposalMetas (proposalHash -> h) = do
  m <- idxGetProposalMeta h
  pure (h, m)

getProposalVotes
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m [AT.ProposalVote]
getProposalVotes periodId = do
  bakers <- idxVoters periodId
  period <- idxPeriod periodId
  prs <- Map.fromList <$> (idxGetProposals (periodInfoEpoch period) >>= mapM getProposalMetas)
  case kind period of
    Proposing ->
      -- In proposal period, we count the number of proposal operations to get the proposal
      -- vote count for the period.
      (fmap (convertProposalVoteFromOperation prs bakers)) <$> (idxProposalOperationsForPeriod periodId)
    _ ->
      -- In other periods, we use the ballots that were submitted during the period.
      (fmap (convertProposalVote prs bakers)) <$> (idxBallotsForPeriod periodId)

getProposals
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m [AT.Proposal]
getProposals pId = do
  vp <- idxPeriod pId
  prs <- idxGetProposals (periodInfoEpoch vp)
  mapM convertProposal (sortBy (\a b -> compare (proposalRolls b, proposalHash b) (proposalRolls a, proposalHash a)) prs)

getProposal
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.ProposalHash
  -> m AT.Proposal
getProposal proposalHash_ = do
  p <- idxGetProposal proposalHash_
  convertProposal p

getNonVoters
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m [AT.Baker]
getNonVoters periodId = do
  periodVoters <- idxVoters periodId
  voters <- (S.fromList . fmap (address . ballotDelegate)) <$> (idxBallotsForPeriod periodId)
  let nonVoters = Prelude.filter (\pv -> S.notMember (address $ periodVoterDelegate pv) voters) periodVoters
  pure $ convertPeriodVoter <$> nonVoters

convertPeriodVoter
  :: PeriodVoter
  -> AT.Baker
convertPeriodVoter pv = AT.Baker
  { _bkPkh = address $ periodVoterDelegate pv
  , _bkRolls = periodVoterRolls pv
  , _bkName = let
      d = periodVoterDelegate pv
      in fromMaybe "" (delegateAlias d)
  , _bkLogoUrl = logoUrl $ periodVoterDelegate pv
  , _bkProfileUrl = Just $ "https://tzkt.io/" <> (AT.hashToText $ address $ periodVoterDelegate pv) <>"/operations/voting"
  }

convertBallot
  :: [PeriodVoter]
  -> Ballot
  -> AT.Ballot
convertBallot bakers b@(Ballot {vote, hash, timestamp, id=id_}) = let
  sender = address $ ballotDelegate b
  _bId = fromIntegral id_
  _bAuthor = case find (\PeriodVoter {..} -> address delegate == sender) bakers of
    Just pr -> convertPeriodVoter pr
    Nothing -> error  "Didn't find period voter"
  _bOperation = hash
  _bTimestamp = timestamp
  _bDecision = vote
  in AT.Ballot {..}

convertProposalVoteFromOperation
  :: Map AT.ProposalHash (Maybe ProposalMeta)
  -> [PeriodVoter]
  -> ProposalOperation
  -> AT.ProposalVote
convertProposalVoteFromOperation metas bakers o@(ProposalOperation {proposal, hash, timestamp, id=id_}) = let
  sender = address $ proposalOperationDelegate o
  _pvId = fromIntegral id_
  _pvProposal = simpleProposalHash proposal
  _pvProposalTitle = do
    meta <- join $ Map.lookup (simpleProposalHash proposal) metas
    pure $ pmtDiscourseTitle meta
  _pvAuthor = case find (\PeriodVoter {delegate} -> address delegate == sender) bakers of
    Just pr -> convertPeriodVoter pr
    Nothing -> error "Proposal operation sender not found"
  _pvOperation = hash
  _pvTimestamp = timestamp
  in AT.ProposalVote {..}

convertProposalVote
  :: HasCallStack
  => Map AT.ProposalHash (Maybe ProposalMeta)
  -> [PeriodVoter]
  -> Ballot
  -> AT.ProposalVote
convertProposalVote metas bakers b@(Ballot {proposal, hash, timestamp, id=id_}) = let
  sender = address $ ballotDelegate b
  _pvId = fromIntegral id_
  _pvProposal = simpleProposalHash proposal
  _pvProposalTitle = do
    meta <- join $ Map.lookup (simpleProposalHash proposal) metas
    pure $ pmtDiscourseTitle meta
  _pvAuthor = case find (\PeriodVoter {delegate} -> address delegate == sender) bakers of
    Just pr -> convertPeriodVoter pr
    Nothing -> error ("No proposal voter found for address:" <> (show sender) <> " and ballot " <> (AT.hashToText hash))
  _pvOperation = hash
  _pvTimestamp = timestamp
  in AT.ProposalVote {..}

getWonProposalForPeriod_
  :: (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> AT.PeriodId
  -> AT.EpochId
  -> m (Maybe Proposal)
getWonProposalForPeriod_ pId currentPeriod epoch = do
  if currentPeriod > pId
    then do
      ps <- getProposalsForPeriod_ pId epoch
      -- In the bunch of proposals, we consider the proposal
      -- with max value of lastPeriod to have won.
      -- We cannot use the "accepted" field because, if a proposal
      -- get rejected in a further period, it will have a non-accepted
      -- status, even though it has survived this period.
      pure . safeHead $ sortBy (\a b -> compare (lastPeriod b) (lastPeriod a)) ps
    else (pure Nothing)

getProposalsForPeriod_
  :: (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> AT.EpochId
  -> m [Proposal]
getProposalsForPeriod_ pId epoch = do
  ps <- idxGetProposals epoch
  pure (Prelude.filter (\Proposal {firstPeriod} -> firstPeriod == pId) ps)

getWonProposalForPeriod
  :: (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> AT.PeriodId
  -> AT.EpochId
  -> m Proposal
getWonProposalForPeriod pId currentPeriod epoch = do
  getWonProposalForPeriod_ pId currentPeriod epoch >>= \case
    Just p -> pure p
    _      -> error "Did not find accepted proposal in period"

askDiscourseHost :: AgoraWorkMode m => m Text
askDiscourseHost = fmt . build <$> fromAgoraConfig (sub #discourse . option #host)

convertProposal
  :: (HasCallStack, AgoraWorkMode m)
  => Proposal
  -> m AT.Proposal
convertProposal p = do
  let pId = firstPeriod p
  bakers <- idxVoters pId
  let proposer = case find (\pv -> (address (periodVoterDelegate pv) == address (initiator p))) bakers of
        Just pr -> convertPeriodVoter pr
        Nothing -> error $
          "Proposer not found in period voters:" <> (show bakers) <> "| " <> ( show $ initiator p)
  (idxProposalOperation (proposalHash p)) >>= \case
    [] -> error "Proposal info couldn't be looked up"
    (proposalOperation:_) -> do
      constants <- getConstantsForLevel ((level :: Operation -> AT.Level) proposalOperation)
      mMetadata <- idxGetProposalMeta (proposalHash p)
      discourseHost <- askDiscourseHost
      pure AT.Proposal
            { _prPeriod = pId
            , _prHash = proposalHash p
            , _prTitle = pmtDiscourseTitle <$> mMetadata
            , _prShortDescription = pmtDiscourseShortDesc <$> mMetadata
            , _prLongDescription = pmtDiscourseLongDesc <$> mMetadata
            , _prTimeCreated = operationTimestamp proposalOperation
            , _prProposalFile = join $ pmtDiscourseFileLink <$> mMetadata
            , _prDiscourseLink = sl discourseHost <$> mMetadata
            , _prVotesCasted = fromIntegral $ proposalRolls p
            , _prVotersNum = fromIntegral $ length bakers
            , _prMinQuorum = fromIntegral $ 100 * (fromMaybe 0 (proposalQuorum constants))
            , _prProposer = proposer
            }
    where
      sl h ProposalMeta {..} = h <> "/t/" <> (show (fromIntegral pmtDiscourseTopicId :: Integer))

getConstantsForLevel
  :: (HasCallStack, AgoraWorkMode m)
  => AT.Level
  -> m Constants
getConstantsForLevel level
  | level < 2 = constants <$> (getProtocolForLevel 2) -- There are no meaningful constants below level 2
  | otherwise = constants <$> (getProtocolForLevel level)

getProtocolForLevel
  :: (HasCallStack, AgoraWorkMode m)
  => AT.Level
  -> m Protocol
getProtocolForLevel level = do
  ps <- idxProtocols
  case find (\Protocol {..} -> (firstLevel <= level) && (not $ isLevelPast lastLevel)) ps of
    Just p  -> pure p
    Nothing -> error $ "Protocol not found for level" <> (show (level, ps))
  where
    isLevelPast Nothing   = False
    isLevelPast (Just ll) = level > ll

getBallotStatsForPeriod
  :: (HasCallStack, AgoraWorkMode m)
  => AT.EpochId
  -> AT.PeriodId
  -> m (Maybe AT.Ballots)
getBallotStatsForPeriod epochIdx periodId = do
  epochInfo <- idxEpoch epochIdx
  pure $ do
    PeriodInfo {..} <- find (\PeriodInfo {..} -> index == periodId) (periods epochInfo)
    _bYay <- fromIntegral <$> yayRolls
    _bNay <- fromIntegral <$> nayRolls
    _bPass <- fromIntegral <$> passRolls
    _bSupermajority <- supermajority
    _bQuorum <- ballotsQuorum
    pure AT.Ballots {..}

getProposalVotesStatForProposalPeriod
  :: (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m AT.VoteStats
getProposalVotesStatForProposalPeriod pId = do
  voters <- idxVoters pId
  proposalOperations <- idxProposalOperationsForPeriod pId

  -- A delegate can vote multiple times. But their rolls for each vote remains
  -- the same in the period. Here we count the proposal operations sent by the delegate
  -- in the period. If they have sent multiple operations, we only count their vote and rolls
  -- only once.
  let delegatesRollsSet =
        S.fromList
          $ (\p -> (proposalOperationDelegate p, proposalOperationRolls p)) <$> proposalOperations

  let votesAvailable = sum (periodVoterRolls <$> voters)
  let votesCast = sum (snd <$> (S.toList delegatesRollsSet))

  let numVoters = fromIntegral $ S.size delegatesRollsSet
  let numVotersTotals = fromIntegral $ length voters

  pure $ AT.VoteStats (fromIntegral votesCast) (fromIntegral votesAvailable) numVoters numVotersTotals

getVotesStat
  :: (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m AT.VoteStats
getVotesStat pId = do
  voters <- idxVoters pId
  ballots <- idxBallotsForPeriod pId

  let ballotDelegatesSet = S.fromList $ ballotDelegate <$> ballots

  let votesAvailable = sum (periodVoterRolls <$> voters)

  -- To be precise, it appears that we should count multiple votes
  -- from the same delegate for the same proposal only once. But the current
  -- behavior (With built in indexer) appears not to do so.
  --
  -- For period 17, the current implementation shows 57,818 votes. But
  -- with duplicates elimination it only shows under 50K votes.
  let votesCast = sum (ballotRolls <$> ballots)

  let numVoters = fromIntegral $ S.size ballotDelegatesSet
  let numVotersTotals = fromIntegral $ length voters

  pure $ AT.VoteStats (fromIntegral votesCast) (fromIntegral votesAvailable) numVoters numVotersTotals

toAgoraPeriodType :: PeriodType -> AT.PeriodType
toAgoraPeriodType Testing     = AT.Testing
toAgoraPeriodType Proposing   = AT.Proposing
toAgoraPeriodType Exploration = AT.Exploration
toAgoraPeriodType Promotion   = AT.Promotion
toAgoraPeriodType Adoption    = AT.Adoption
