-- | Endpoints of the Tzkt indexer Api.

module Agora.Indexer.Api
  ( IndexerEndpoints (..)
  ) where


import Servant.API (Capture, Get, JSON, QueryParam, (:>))
import Servant.API.Generic ((:-))

import Agora.Indexer.Types
import qualified Agora.Types as AT

-- | Definition of a subset of Tezos node RPC API.
data IndexerEndpoints route = IndexerEndpoints
  { neLevel_ :: route -- Get info about a given block
      :- "blocks"
      :> Capture "level" AT.Level
      :> QueryParam "select" Text
      :> Get '[JSON] Block
  , neHead_ :: route -- Get info about head block
      :- "head"
      :> Get '[JSON] Head
  , neBallotsForProposal_ :: route -- Get ballots for a specific proposal irrespective of the period
      :- "operations"
      :> "ballots"
      :> QueryParam "proposal" AT.ProposalHash
      :> Get '[JSON] [Ballot]
  , neBallotsForPeriod_ :: route -- Get ballots for a specific period
      :- "operations"
      :> "ballots"
      :> QueryParam "period" AT.PeriodId
      :> QueryParam "offset" Int
      :> QueryParam "limit" Int
      :> Get '[JSON] [Ballot]
  , neVoters_ :: route -- Get voters for a given period
      :- "voting"
      :> "periods"
      :> Capture "periodId" AT.PeriodId
      :> "voters"
      :> QueryParam "offset" Int
      :> QueryParam "limit" Int
      :> Get '[JSON] [PeriodVoter]
  , neBlockCount_ :: route -- Get the total number of blocks yet
      :- "blocks"
      :> "count"
      :> Get '[JSON] AT.Level
  , neGetProposal_ :: route -- Get info about proposal
      :- "voting"
      :> "proposals"
      :> Capture "hash" AT.ProposalHash
      :> Get '[JSON] Proposal
  , neGetProposals_ :: route -- Get info about all proposals in a voting epoch
      :- "voting"
      :> "proposals"
      :> QueryParam "epoch" AT.EpochId
      :> Get '[JSON] [Proposal]
  , neProposalOperation_ :: route -- Get all proposal operations for the given proposal (Multiple bakers might inject same proposal)
      :- "operations"
      :> "proposals"
      :> QueryParam "proposal" AT.ProposalHash
      :> QueryParam "offset" Int
      :> QueryParam "limit" Int
      :> Get '[JSON] [Operation]
  , neProposalOperationsForPeriod_ :: route -- Get all proposal operations for the given period
      :- "operations"
      :> "proposals"
      :> QueryParam "period" AT.PeriodId
      :> QueryParam "offset" Int
      :> QueryParam "limit" Int
      :> Get '[JSON] [ProposalOperation]
  , neProtocols_ :: route -- Get info about the protocol
      :- "protocols"
      :> QueryParam "offset" Int
      :> QueryParam "limit" Int
      :> Get '[JSON] [Protocol]
  , neEpoch_ :: route -- Get info about the voting epoch
      :- "voting"
      :> "epochs"
      :> Capture "index" AT.EpochId
      :> Get '[JSON] EpochInfo
  , neEpochs_ :: route -- Get info about the voting epoch
      :- "voting"
      :> "epochs"
      :> Get '[JSON] [EpochInfo]
  } deriving Generic

