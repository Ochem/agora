{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE Strict                #-}
-- | This module holds the types that represent the data exported
--   by the Tzkt indexer Api.

module Agora.Indexer.Types
  ( Ballot(..)
  , Block(..)
  , Constants(..)
  , Delegate(..)
  , EpochInfo(..)
  , Head(..)
  , Operation(..)
  , PeriodInfo(..)
  , PeriodStatus(..)
  , PeriodType(..)
  , PeriodVoter(..)
  , Proposal(..)
  , ProposalOperation(..)
  , ProposalStatus(..)
  , Protocol(..)
  , SimpleProposal(..)
  , VotingPeriod(..)
  , ballotDelegate
  , ballotRolls
  , blockTimestamp
  , delegateAlias
  , epochIndex
  , headLevel
  , operationDelegate
  , operationTimestamp
  , periodIndex
  , periodInfoEpoch
  , periodVoterDelegate
  , periodVoterRolls
  , proposalHash
  , proposalOperationDelegate
  , proposalOperationRolls
  , proposalOperationProposal
  , proposalRolls
  , simpleProposalHash
  ) where

import Data.Aeson
import Data.Time.Clock (UTCTime)

import qualified Agora.Types as AT
import Agora.Util

type Number = Int32

data ProposalStatus
  = Active -- the proposal in the active state
  | Accepted -- the proposal was accepted
  | Rejected -- the proposal was rejected due to too many "nay" ballots
  | Skipped -- the proposal was skipped due to the quorum was not reached
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance TagEnum ProposalStatus where
  enumDesc _ = "Proposal status"
  toTag Active   = "active"
  toTag Accepted = "accepted"
  toTag Rejected = "rejected"
  toTag Skipped  = "skipped"

instance FromJSON ProposalStatus where
  parseJSON = parseJSONTag

instance ToJSON ProposalStatus where
  toJSON = toJSONTag

data PeriodStatus
  = PActive
  | PNoProposals
  | PNoQuorum
  | PNoSupermajority
  | PSuccess
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance TagEnum PeriodStatus where
  enumDesc _ = "Period status"
  toTag PActive          = "active"
  toTag PNoProposals     = "no_proposals"
  toTag PNoQuorum        = "no_quorum"
  toTag PNoSupermajority = "no_supermajority"
  toTag PSuccess         = "success"

instance FromJSON PeriodStatus where
  parseJSON = parseJSONTag

instance ToJSON PeriodStatus where
  toJSON = toJSONTag

data PeriodType
  = Proposing     -- ^ Proposal phase (named `Proposing` to avoid name clashes with @Proposal@ datatype)
  | Exploration   -- ^ Exploration phase
  | Testing       -- ^ Testing phase
  | Promotion     -- ^ Promotion phase
  | Adoption      -- ^ Adoption phase
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance TagEnum PeriodType where
  enumDesc _ = "Period type"
  toTag Proposing   = "proposal"
  toTag Exploration = "exploration"
  toTag Testing     = "testing"
  toTag Promotion   = "promotion"
  toTag Adoption    = "adoption"

instance FromJSON PeriodType where
  parseJSON = parseJSONTag

instance ToJSON PeriodType where
  toJSON = toJSONTag

data PeriodInfo = PeriodInfo
  { kind          :: PeriodType
  , firstLevel    :: AT.Level
  , lastLevel     :: AT.Level
  , startTime     :: UTCTime
  , endTime       :: UTCTime
  , status        :: Maybe PeriodStatus
  , epoch         :: AT.EpochId
  , yayBallots    :: Maybe Number
  , yayRolls      :: Maybe AT.Rolls
  , nayBallots    :: Maybe Number
  , nayRolls      :: Maybe AT.Rolls
  , passBallots   :: Maybe Number
  , passRolls     :: Maybe AT.Rolls
  , ballotsQuorum :: Maybe Float
  , supermajority :: Maybe Float
  , totalBakers   :: Maybe Number
  , totalRolls    :: Maybe AT.Rolls
  , index         :: AT.PeriodId
    -- An Epoch is a sequence of voting periods that comprises of one
    -- Tezos amendment. So it will always have one period (proposal) and
    -- possible contain further periods, util the proposal is rejected or
    -- activated.
  } deriving stock (Eq, Show, Generic)

data Protocol = Protocol
  { hash       :: AT.ProtocolHash
  , firstLevel :: AT.Level
  , lastLevel  :: Maybe AT.Level
  , constants  :: Constants
  } deriving (Show, Generic)

data Constants = Constants
  { proposalQuorum  :: Maybe Number
  , blocksPerCycle  :: Maybe Number
  , blocksPerVoting :: Maybe Number
  } deriving (Show, Generic)

data Head = Head
  { level     :: AT.Level
  , hash      :: AT.BlockHash
  , protocol  :: AT.ProtocolHash
  , timestamp :: UTCTime
  } deriving (Eq, Show, Generic)

data Block = Block
  { level     :: AT.Level
  , timestamp :: UTCTime
  } deriving (Eq, Show, Generic)

data Delegate = Delegate
  { address :: AT.PublicKeyHash
  , alias   :: Maybe Text
  , logoUrl :: Maybe Text
  } deriving (Show, Eq, Ord, Generic)

data Ballot = Ballot
  { delegate  :: Delegate
  , vote      :: AT.Decision
  , rolls     :: AT.Rolls
  , id        :: Number
  , hash      :: AT.OperationHash
  , timestamp :: UTCTime
  , proposal  :: SimpleProposal
  } deriving (Eq, Show, Generic)

data SimpleProposal = SimpleProposal
  { alias :: Maybe Text
  , hash  :: AT.ProposalHash
  } deriving (Show, Generic)

instance Ord SimpleProposal where
  compare a b = compare (simpleProposalHash a) (simpleProposalHash b)

instance Eq SimpleProposal where
  a == b = (simpleProposalHash a) == (simpleProposalHash b)

data EpochInfo = EpochInfo
  { index   :: AT.EpochId
  , periods :: [PeriodInfo]
  } deriving (Eq, Show, Generic)

data PeriodVoter = PeriodVoter
  { delegate :: Delegate
  , rolls    :: AT.Rolls
  } deriving (Eq, Show, Generic)

data Operation = Operation
  { timestamp :: UTCTime
  , level     :: AT.Level
  , hash      :: AT.OperationHash
  , proposal  :: Maybe SimpleProposal
  , delegate  :: Delegate
  } deriving (Eq, Show, Generic)

data Proposal = Proposal
  { epoch       :: AT.EpochId
  , rolls       :: AT.Rolls
  , hash        :: AT.ProposalHash
  , firstPeriod :: AT.PeriodId
  , lastPeriod  :: AT.PeriodId
  , initiator   :: Delegate
  , status      :: ProposalStatus
  } deriving (Eq, Show, Generic)

data ProposalOperation = ProposalOperation
  { id        :: Number
  , hash      :: AT.OperationHash
  , proposal  :: SimpleProposal
  , timestamp :: UTCTime
  , rolls     :: AT.Rolls
  , delegate  :: Delegate
  } deriving (Eq, Show, Generic)

data VotingPeriod = VotingPeriod
  { totalBakers    :: Maybe Number
  , proposalsCount :: Maybe Number
  , yayBallots     :: Maybe Number
  , nayBallots     :: Maybe Number
  , yayRolls       :: Maybe Number
  , nayRolls       :: Maybe Number
  , passRolls      :: Maybe Number
  , passBallots    :: Maybe Number
  , totalRolls     :: Maybe Number
  } deriving (Show, Generic)

instance FromJSON Protocol where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Protocol where
  toJSON = genericToJSON defaultOptions

instance FromJSON Operation where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Operation where
  toJSON = genericToJSON defaultOptions

instance FromJSON SimpleProposal where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON SimpleProposal where
  toJSON = genericToJSON defaultOptions

instance FromJSON EpochInfo where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON EpochInfo where
  toJSON = genericToJSON defaultOptions

instance FromJSON Constants where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Constants where
  toJSON = genericToJSON defaultOptions

instance FromJSON Block where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Block where
  toJSON = genericToJSON defaultOptions

instance FromJSON Head where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Head where
  toJSON = genericToJSON defaultOptions

instance FromJSON Ballot where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Ballot where
  toJSON = genericToJSON defaultOptions

instance FromJSON Delegate where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Delegate where
  toJSON = genericToJSON defaultOptions

instance FromJSON PeriodInfo where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON PeriodInfo where
  toJSON = genericToJSON defaultOptions

instance FromJSON PeriodVoter where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON PeriodVoter where
  toJSON = genericToJSON defaultOptions

instance FromJSON ProposalOperation where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON ProposalOperation where
  toJSON = genericToJSON defaultOptions

instance FromJSON Proposal where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Proposal where
  toJSON = genericToJSON defaultOptions

instance FromJSON VotingPeriod where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON VotingPeriod where
  toJSON = genericToJSON defaultOptions

ballotDelegate :: Ballot -> Delegate
ballotDelegate = delegate

ballotRolls :: Ballot -> AT.Rolls
ballotRolls = rolls

proposalOperationRolls :: ProposalOperation -> AT.Rolls
proposalOperationRolls = rolls

proposalOperationDelegate :: ProposalOperation -> Delegate
proposalOperationDelegate = delegate

proposalOperationProposal :: ProposalOperation -> SimpleProposal
proposalOperationProposal = proposal

proposalHash :: Proposal -> AT.ProposalHash
proposalHash = hash

simpleProposalHash :: SimpleProposal -> AT.ProposalHash
simpleProposalHash = hash

headLevel :: Head -> AT.Level
headLevel = level

blockTimestamp :: Block -> UTCTime
blockTimestamp = timestamp

operationTimestamp :: Operation -> UTCTime
operationTimestamp = timestamp

operationDelegate :: Operation -> Delegate
operationDelegate = delegate

periodInfoEpoch :: PeriodInfo -> AT.EpochId
periodInfoEpoch = epoch

periodVoterDelegate :: PeriodVoter -> Delegate
periodVoterDelegate = delegate

periodVoterRolls :: PeriodVoter -> AT.Rolls
periodVoterRolls = rolls

periodIndex :: PeriodInfo -> AT.PeriodId
periodIndex = index

epochIndex :: EpochInfo -> AT.EpochId
epochIndex = index

proposalRolls :: Proposal -> AT.Rolls
proposalRolls = rolls

delegateAlias :: Delegate -> Maybe Text
delegateAlias = alias
