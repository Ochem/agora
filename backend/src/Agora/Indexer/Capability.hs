-- | This module implements a capability that caches data fetched
-- from the indexer, the baker api and discourse and exposes it to the endpoint
-- handlers.
--
-- The interface to the actual indexer is wrapped in functions in the
-- `IndexerEndpointsIO` record.
--
-- This is passed to the `withIndexer` which implements a capability that talks
-- to the indexer using these function from `IndexerEndpointsIO`, and also
-- caches the results. It also periodically refreshes this cache. The methods
-- in this capability directly return data stored in the cache.
{-# LANGUAGE Strict #-}

module Agora.Indexer.Capability
  ( IndexerCap(..)
  , MonadIndexerCap(..)
  , IndexerEndpointsIO(..)
  , getClientEnv
  , getIndexerReal
  , withIndexer
  , ProposalMeta(..)
  ) where

import Control.Monad.Reader (withReaderT)
import Data.Map as Map
import Data.Time.Units (Second, toMicroseconds)
import Fmt ((+|), (|+))
import Loot.Log (Logging, logDebug, logInfo)
import Monad.Capabilities (CapImpl(..), CapsT, HasCaps, HasNoCap, addCap, makeCap)
import Servant.Client (ClientEnv, ClientM, runClientM)
import Servant.Client.Generic (genericClient)
import UnliftIO (MonadUnliftIO)
import qualified UnliftIO as UIO
import qualified UnliftIO.Concurrent as UIO

import Agora.BakerFetch.Api
import qualified Agora.BakerFetch.Types as BF
import Agora.Config.Definition
import Agora.Discourse.Client
  (DiscourseClient, getDiscoursePost, getDiscourseTopic, getProposalTopic, postProposalStubAsync)
import Agora.Discourse.Html
import Agora.Discourse.Types
import Agora.Indexer.Api
import Agora.Indexer.Types as AI
import Agora.Metrics.Capability
import qualified Agora.Types as AT
import Agora.Util

type IndexerDeps = '[BakerFetchCap, DiscourseClient, AgoraConfigCap, MetricsCap, Logging]

type HasIndexerDeps caps = (HasCallStack, HasCaps IndexerDeps caps)

type IndexerM caps = CapsT caps AT.Base

data ProposalMeta = ProposalMeta
  { pmtDiscourseTopicId   :: AT.DiscourseTopicId
  , pmtDiscoursePostId    :: AT.DiscoursePostId
  , pmtDiscourseTitle     :: Text
  , pmtDiscourseShortDesc :: Text
  , pmtDiscourseLongDesc  :: Text
  , pmtDiscourseFileLink  :: Maybe Text
  } deriving stock Show

data IndexerEndpointsIO = IndexerEndpointsIO
  { neLevel :: AT.Level -> Maybe Text -> IO Block
  , neHead :: IO Head
  , neBallotsForProposal :: Maybe AT.ProposalHash -> IO [Ballot]
  , neBallotsForPeriod :: Maybe AT.PeriodId -> Maybe Int -> Maybe Int -> IO [Ballot]
  , neVoters :: AT.PeriodId -> Maybe Int -> Maybe Int -> IO [PeriodVoter]
  , neBlockCount :: IO AT.Level
  , neGetProposal :: AT.ProposalHash -> IO Proposal
  , neGetProposals :: Maybe AT.EpochId -> IO [Proposal]
  , neProposalOperation :: Maybe AT.ProposalHash -> Maybe Int -> Maybe Int -> IO [Operation]
  , neProposalOperationsForPeriod :: Maybe AT.PeriodId -> Maybe Int -> Maybe Int -> IO [ProposalOperation]
  , neProtocols :: Maybe Int -> Maybe Int -> IO [Protocol]
  , neEpoch :: AT.EpochId -> IO EpochInfo
  , neEpochs :: IO [EpochInfo]
  }

getIndexerReal :: forall m. MonadIO m => ClientEnv -> m IndexerEndpointsIO
getIndexerReal env = do
  let
    runEndpoint :: forall a. ClientM a -> IO a
    runEndpoint c =
        liftIO $ runClientM c env >>= \case
          Right a  -> pure a
          Left err -> error (show err)
  let IndexerEndpoints {..} = genericClient
  pure $ IndexerEndpointsIO
    { neLevel = \level select -> (runEndpoint (neLevel_ level select))
    , neHead = runEndpoint (neHead_ :: ClientM Head)
    , neBallotsForProposal = \pHash -> runEndpoint (neBallotsForProposal_ pHash)
    , neBallotsForPeriod = \periodId o l -> runEndpoint (neBallotsForPeriod_ periodId o l)
    , neBlockCount = runEndpoint neBlockCount_
    , neGetProposals = \mepochId -> runEndpoint (neGetProposals_ mepochId)
    , neVoters = \pid mo ml -> runEndpoint (neVoters_ pid mo ml)
    , neGetProposal = \phash -> runEndpoint (neGetProposal_ phash)
    , neProposalOperation = \mpid mof ml -> runEndpoint (neProposalOperation_ mpid mof ml)
    , neProposalOperationsForPeriod = \mpid mof ml -> runEndpoint (neProposalOperationsForPeriod_ mpid mof ml)
    , neProtocols = \mof ml -> runEndpoint (neProtocols_ mof ml)
    , neEpochs = runEndpoint neEpochs_
    , neEpoch = \eIdx -> runEndpoint (neEpoch_ eIdx)
    }

data IndexerCap m = IndexerCap
  { _idxPeriods                     :: m [PeriodInfo]
  , _idxPeriod                      :: AT.PeriodId -> m PeriodInfo
  , _idxLevel                       :: AT.Level -> m Block
  , _idxHead                        :: m Head
  , _idxBallotsForProposal          :: AT.ProposalHash -> m [Ballot]
  , _idxBallotsForPeriod            :: AT.PeriodId -> m [Ballot]
  , _idxVoters                      :: AT.PeriodId -> m [PeriodVoter]
  , _idxBlockCount                  :: m AT.Level
  , _idxGetProposal                 :: AT.ProposalHash -> m Proposal
  , _idxGetProposals                :: AT.EpochId -> m [Proposal]
  , _idxGetProposalMeta             :: AT.ProposalHash -> m (Maybe ProposalMeta)
  , _idxProposalOperation           :: AT.ProposalHash -> m [Operation]
  , _idxProposalOperationsForPeriod :: AT.PeriodId -> m [ProposalOperation]
  , _idxProtocols                   :: m [Protocol]
  , _idxEpoch                       :: AT.EpochId -> m EpochInfo
  }

makeCap ''IndexerCap

data CacheVars = CacheVars
  { chPeriodInfos :: TVar (Map AT.PeriodId PeriodInfo)
  , chLevels :: TVar (Map AT.Level Block)
  , chHead :: TVar Head
  , chBallotsForProposal :: TVar (Map AT.ProposalHash [Ballot])
  , chBallotsForPeriod :: TVar (Map AT.PeriodId [Ballot])
  , chVotersForPeriod :: TVar (Map AT.PeriodId [PeriodVoter])
  , chBlockCount :: TVar AT.Level
  , chProposals :: TVar (Map AT.ProposalHash Proposal)
  , chProposalsForEpoch :: TVar (Map AT.EpochId [Proposal])
  , chProposalOperationsForProposal :: TVar (Map AT.ProposalHash [Operation])
  , chProposalOperationsForPeriod :: TVar (Map AT.PeriodId [ProposalOperation])
  , chProtocols :: TVar [Protocol]
  , chEpoch :: TVar (Map AT.EpochId EpochInfo)
  , chBakers :: TVar (Map AT.PublicKeyHash Delegate)
  , chProposalMeta :: TVar (Map AT.ProposalHash ProposalMeta)
  }

copyCacheVars :: CacheVars -> CacheVars -> IO ()
copyCacheVars src dst = atomically $ do
  readTVar (chPeriodInfos src) >>= writeTVar (chPeriodInfos dst)
  readTVar (chLevels src) >>= writeTVar (chLevels dst)
  readTVar (chHead src) >>= writeTVar (chHead dst)
  readTVar (chBallotsForProposal src) >>= writeTVar (chBallotsForProposal dst)
  readTVar (chBallotsForPeriod src) >>= writeTVar (chBallotsForPeriod dst)
  readTVar (chVotersForPeriod src) >>= writeTVar (chVotersForPeriod dst)
  readTVar (chBlockCount src) >>= writeTVar (chBlockCount dst)
  readTVar (chProposals src) >>= writeTVar (chProposals dst)
  readTVar (chProposalsForEpoch src) >>= writeTVar (chProposalsForEpoch dst)
  readTVar (chProposalOperationsForProposal src) >>= writeTVar (chProposalOperationsForProposal dst)
  readTVar (chProposalOperationsForPeriod src) >>= writeTVar (chProposalOperationsForPeriod dst)
  readTVar (chProtocols src) >>= writeTVar (chProtocols dst)
  readTVar (chEpoch src) >>= writeTVar (chEpoch dst)
  readTVar (chBakers src) >>= writeTVar (chBakers dst)
  readTVar (chProposalMeta src) >>= writeTVar (chProposalMeta dst)

initCacheVars :: HasIndexerDeps caps => IndexerEndpointsIO -> IndexerM caps CacheVars
initCacheVars indexer = do
  head_ <- liftIO $ neHead indexer
  blockCount <- liftIO $ neBlockCount indexer

  chPeriodInfos <- newTVarIO mempty
  chLevels <- newTVarIO mempty
  chVotersForPeriod <- newTVarIO mempty
  chHead <- newTVarIO head_
  chBallotsForProposal <- newTVarIO mempty
  chBallotsForPeriod <- newTVarIO mempty
  chBlockCount <- newTVarIO blockCount
  chProposals <- newTVarIO mempty
  chProposalsForEpoch <- newTVarIO mempty
  chProposalOperationsForProposal <- newTVarIO mempty
  chProposalOperationsForPeriod <- newTVarIO mempty
  chProtocols <- newTVarIO mempty
  chEpoch <- newTVarIO mempty
  chBakers <- newTVarIO mempty
  chProposalMeta <- newTVarIO mempty
  pure CacheVars {..}

atomicUpdateIndex :: forall m k v. (Ord k, MonadUnliftIO m) => TVar (Map k v) -> k -> v -> m ()
atomicUpdateIndex t k v =
  atomically $ do
    existing <- readTVar t
    writeTVar t (Map.insert k v existing)

-- Preserves and restores the last state of cache in case of an exception
-- during refresh.
refreshCacheVarsSafe
  :: HasIndexerDeps caps
  => IndexerEndpointsIO
  -> CacheVars
  -> CacheVars
  -> IndexerM caps ()
refreshCacheVarsSafe idxEp cv cvCopy = do
  let retryIn = 30
  let retryInInt = fromIntegral retryIn :: Int
  let reportError e = do
        let message = "Error: "+|displayException e|+ ", happened in during cache refresh. Retrying in "+|retryInInt|+" seconds."
        sendMetrics $ CriticalError message
  suppressException @SomeException retryIn reportError $ do
    liftIO $ copyCacheVars cv cvCopy
    refreshCacheVars idxEp cvCopy
    liftIO $ copyCacheVars cvCopy cv

refreshCacheVars
  :: forall caps. HasIndexerDeps caps
  => IndexerEndpointsIO
  -> CacheVars
  -> IndexerM caps ()
refreshCacheVars indexer CacheVars {..} = do
  logDebug "Refreshing cache..."
  head_ <- liftIO $ neHead indexer
  sendMetrics $ ProcessedBlock $ headLevel head_
  blockCount <- liftIO $ neBlockCount indexer

  refreshBakers

  atomically $ writeTVar chHead head_
  atomically $ writeTVar chBlockCount blockCount

  logDebug "Getting epoch info"
  epochInfos <- liftIO $ neEpochs indexer
  mapM_ refreshEpoch epochInfos

  protocols <- liftIO $ (neProtocols indexer) (Just 0) (Just 10000)
  atomically $ writeTVar chProtocols protocols

  where

    refreshEpoch
      :: HasIndexerDeps caps => EpochInfo -> IndexerM caps ()
    refreshEpoch epoch = do
      logDebug ("Refreshing epoch:" +| show epoch)

      let thisEpochIndex = epochIndex epoch
      atomicUpdateIndex chEpoch thisEpochIndex epoch
      mapM_ refreshPeriod (periods epoch)

      proposalsForEpoch <- liftIO $ (neGetProposals indexer) $ Just thisEpochIndex
      atomicUpdateIndex chProposalsForEpoch thisEpochIndex proposalsForEpoch

      mapM_ refreshProposal proposalsForEpoch

    refreshProposal :: HasIndexerDeps caps => Proposal -> IndexerM caps ()
    refreshProposal proposal = do
      let thisProposalHash = proposalHash proposal
      refreshProposalMeta thisProposalHash
      atomicUpdateIndex chProposals thisProposalHash proposal
      (paged ((neProposalOperation indexer) (Just thisProposalHash))) >>=
        atomicUpdateIndex chProposalOperationsForProposal thisProposalHash

      liftIO $ ((neBallotsForProposal indexer) $ Just thisProposalHash) >>=
        atomicUpdateIndex chBallotsForProposal thisProposalHash

    -- Checks if the metadata have discourse topic id and title set. If it does
    refreshProposalMeta :: HasIndexerDeps caps => AT.ProposalHash -> IndexerM caps ()
    refreshProposalMeta pHash = do
      logDebug $ "Refreshing proposal meta for " +| pHash |+ ""
      proposalMetas <- atomically $ readTVar chProposalMeta
      case Map.lookup pHash proposalMetas of
        Just pm -> do
          cooked <- pCooked <$> (getDiscoursePost (pmtDiscoursePostId pm))
          title <- tTitle <$> (getDiscourseTopic (pmtDiscourseTopicId pm))
          let titleWithoutParen = AT.shortenHash pHash
          case toHtmlPartsMaybe titleWithoutParen  <$> (parseHtmlParts cooked) of
              Left e  -> do
                sendMetrics $ Warning $ "Couldn't parse Discourse topic, reason: " +| e |+ ""
              Right hp ->  do
                atomicUpdateIndex chProposalMeta pHash pm
                  { pmtDiscourseTitle = unTitle title
                  , pmtDiscourseLongDesc = fromMaybe "" $ hpLong hp
                  , pmtDiscourseShortDesc = fromMaybe "" $ hpShort hp
                  , pmtDiscourseFileLink = hpFileLink hp
                  }
        Nothing -> do
          getProposalTopic pHash >>= \case
            Just topic -> do
              case parseHtmlParts (pCooked $ tPosts topic) of
                Left e  -> do
                  sendMetrics $ Warning $ "Couldn't parse Discourse topic, reason: " +| e |+ ""
                Right hp ->  let
                  pm = ProposalMeta
                    { pmtDiscourseTopicId = tId topic
                    , pmtDiscoursePostId = pId $ tPosts topic
                    , pmtDiscourseTitle = unTitle $ tTitle topic
                    , pmtDiscourseLongDesc = hpLong hp
                    , pmtDiscourseShortDesc = hpShort hp
                    , pmtDiscourseFileLink = hpFileLink hp
                    } in atomicUpdateIndex chProposalMeta pHash pm
            Nothing    -> do
              logInfo $ "Didn't find existing topic for: " +| pHash |+ ""
              -- Topic does not exist. Just trigger topic creation, and we will sync
              -- DB in the next iteration.

              postProposalStubAsync pHash

    refreshPeriod
      :: HasIndexerDeps caps => PeriodInfo -> IndexerM caps ()
    refreshPeriod periodInfo = do
      logDebug ("Refreshing period:" +| (show $ periodIndex periodInfo) )
      let thisPeriodIndex = periodIndex periodInfo
      atomicUpdateIndex chPeriodInfos thisPeriodIndex periodInfo
      logDebug ("Refreshing ballots for period.")
      (paged $ (neBallotsForPeriod indexer) (Just thisPeriodIndex)) >>=
        atomicUpdateIndex chBallotsForPeriod thisPeriodIndex

      logDebug ("Refreshing voters for period.")
      (paged $ (neVoters indexer) thisPeriodIndex) >>= \voters -> do
        bakers <- atomically $ readTVar chBakers
        atomicUpdateIndex chVotersForPeriod thisPeriodIndex (fillMeta bakers <$> voters)

      logDebug ("Refreshing proposals for period:" +| (show $ periodIndex periodInfo) )
      (paged $ (neProposalOperationsForPeriod indexer) (Just thisPeriodIndex)) >>=
        atomicUpdateIndex chProposalOperationsForPeriod thisPeriodIndex

    biToDelegate :: BF.BakerInfo -> (AT.PublicKeyHash, Delegate)
    biToDelegate BF.BakerInfo {..} =
      (biDelegationCode, Delegate biDelegationCode (Just biBakerName) biLogo)

    fillMeta :: Map.Map AT.PublicKeyHash Delegate -> PeriodVoter -> PeriodVoter
    fillMeta dm pv = let
      pvDelegate = periodVoterDelegate pv
      fullDelegate = case Map.lookup (address pvDelegate) dm of
        Just d  -> d
        Nothing -> pvDelegate
      in PeriodVoter fullDelegate (periodVoterRolls pv)

    refreshBakers
      :: HasIndexerDeps caps => IndexerM caps ()
    refreshBakers = do
      logDebug "Refreshing bakers"
      bakerMap <- (Map.fromList . fmap biToDelegate . BF.bilBakers) <$> getBakers
      atomically $ writeTVar chBakers bakerMap

-- This with implements a capability that talks to the indexer using the
-- function in provided `IndexerEndpointsIO`, and also caches the results. The
-- cache is periodically refreshed and the methods in this capability directly
-- return data stored in the cache.
withIndexer
  :: forall a caps. (HasNoCap IndexerCap caps, HasIndexerDeps caps)
  => IndexerEndpointsIO
  -> Second
  -> CapsT (IndexerCap ': caps) AT.Base a
  -> CapsT caps AT.Base a
withIndexer indexer refreshInterval action = do
  cvr <- initCacheVars indexer
  cvrCopy <- initCacheVars indexer
  UIO.withAsync
    (forever $ do
        refreshCacheVarsSafe indexer cvr cvrCopy
        UIO.threadDelay $ fromIntegral $ toMicroseconds refreshInterval)
    (\_ -> withReaderT (addCap (indexerCap indexer cvr)) action)

lookupCachedIndex :: forall m k v. (MonadUnliftIO m, Ord k) => TVar (Map k v) -> k -> m (Maybe v)
lookupCachedIndex t k = do
  cacheMap <- liftIO $ atomically $ readTVar t
  pure $ Map.lookup k cacheMap

requireCachedIndex :: forall m k v. (HasCallStack, Show k, MonadUnliftIO m, Ord k) => TVar (Map k v) -> k -> m v
requireCachedIndex t k = lookupCachedIndex t k >>= \case
  Just x  -> pure x
  Nothing -> (error $ "Index not found in cache:" <> (show k))

indexerCap :: IndexerEndpointsIO -> CacheVars -> CapImpl IndexerCap IndexerDeps AT.Base
indexerCap idxEps cvr = CapImpl $ IndexerCap
  { _idxPeriods = getPeriods cvr
  , _idxPeriod = getPeriod cvr
  , _idxBlockCount = getBlockCount cvr
  , _idxLevel = getLevel idxEps cvr
  , _idxHead = getHead cvr
  , _idxBallotsForPeriod = ballotsForPeriod cvr
  , _idxBallotsForProposal = ballotsForProposal cvr
  , _idxVoters = votersForPeriod cvr
  , _idxGetProposal = getProposal cvr
  , _idxGetProposals = getProposalsForEpoch cvr
  , _idxGetProposalMeta = getProposalMeta cvr
  , _idxProposalOperation = getProposalOperations cvr
  , _idxProposalOperationsForPeriod = getProposalOperationsForPeriod cvr
  , _idxProtocols = getProtocols cvr
  , _idxEpoch = getEpoch cvr
  }

getEpoch :: HasIndexerDeps caps => CacheVars -> AT.EpochId -> IndexerM caps EpochInfo
getEpoch CacheVars {..} epoch =
  requireCachedIndex chEpoch epoch

getProposalMeta :: HasIndexerDeps caps => CacheVars -> AT.ProposalHash -> IndexerM caps (Maybe ProposalMeta)
getProposalMeta CacheVars {..} pHash =
  lookupCachedIndex chProposalMeta pHash

getProtocols :: HasIndexerDeps caps => CacheVars -> IndexerM caps [Protocol]
getProtocols CacheVars {..} =
  liftIO $ atomically $ readTVar chProtocols

getProposalOperationsForPeriod :: HasIndexerDeps caps => CacheVars -> AT.PeriodId -> IndexerM caps [ProposalOperation]
getProposalOperationsForPeriod CacheVars {..} periodId =
  requireCachedIndex chProposalOperationsForPeriod periodId

getProposalOperations :: HasIndexerDeps caps => CacheVars -> AT.ProposalHash -> IndexerM caps [Operation]
getProposalOperations CacheVars {..} proposalHash_ =
  requireCachedIndex chProposalOperationsForProposal proposalHash_

getProposalsForEpoch :: HasIndexerDeps caps => CacheVars -> AT.EpochId -> IndexerM caps [Proposal]
getProposalsForEpoch CacheVars {..} epoch =
  requireCachedIndex chProposalsForEpoch epoch

getProposal :: (HasCallStack, HasIndexerDeps caps) => CacheVars -> AT.ProposalHash -> IndexerM caps Proposal
getProposal CacheVars {..} proposalHash_ =
  requireCachedIndex chProposals proposalHash_

votersForPeriod :: HasIndexerDeps caps => CacheVars -> AT.PeriodId -> IndexerM caps [PeriodVoter]
votersForPeriod CacheVars {..} periodId =
  requireCachedIndex chVotersForPeriod periodId

ballotsForProposal :: HasIndexerDeps caps => CacheVars -> AT.ProposalHash -> IndexerM caps [Ballot]
ballotsForProposal CacheVars {..} proposalHash_ =
  requireCachedIndex chBallotsForProposal proposalHash_

ballotsForPeriod :: HasIndexerDeps caps => CacheVars -> AT.PeriodId -> IndexerM caps [Ballot]
ballotsForPeriod CacheVars {..} periodId =
  requireCachedIndex chBallotsForPeriod periodId

getHead :: HasIndexerDeps caps => CacheVars -> IndexerM caps Head
getHead CacheVars {..} =
  liftIO $ atomically $ readTVar chHead

getLevel :: HasIndexerDeps caps => IndexerEndpointsIO -> CacheVars -> AT.Level -> IndexerM caps Block
getLevel indexer CacheVars {..} level = do
  cachedLevels <- liftIO $ atomically $ readTVar chLevels
  case Map.lookup level cachedLevels of
    Just l -> pure l
    Nothing -> do
      l <- (liftIO $ (neLevel indexer) level (Just "level,timestamp"))
      atomicUpdateIndex chLevels level l
      pure l

getBlockCount :: MonadUnliftIO m => CacheVars -> m AT.Level
getBlockCount CacheVars {..} =
  liftIO $ atomically $ readTVar chBlockCount

getPeriod :: MonadUnliftIO m => CacheVars -> AT.PeriodId -> m PeriodInfo
getPeriod CacheVars {..} periodId = requireCachedIndex chPeriodInfos periodId

getPeriods :: MonadUnliftIO m => CacheVars -> m [PeriodInfo]
getPeriods CacheVars {..} = do
  periodInfoMap <- liftIO $ atomically $ readTVar chPeriodInfos
  pure $ Map.elems periodInfoMap

-- Aggregate result from paged request until the request respond with an
-- empty result.
paged
  :: forall a caps . (HasIndexerDeps caps) => (Maybe Int -> Maybe Int -> IO [a])
  -> IndexerM caps [a]
paged fn = pagedRequest_ mempty 0
  where
    pagedRequest_ :: [a] -> Int -> IndexerM caps [a]
    pagedRequest_ last_ offset = do
      (liftIO $ fn (Just offset) (Just chunkSize)) >>= \case
        []  -> do
          logInfo "No more pages"
          pure last_
        new -> do
          logInfo ("Getting offset:" +| show offset)
          pagedRequest_ (last_ <> new) (offset + length new)

    chunkSize = 100
