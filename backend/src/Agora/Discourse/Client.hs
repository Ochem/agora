{-# LANGUAGE TypeOperators #-}

module Agora.Discourse.Client
       ( DiscourseClient (..)
       , DiscourseClientRaw (..)
       , DiscourseError (..)
       , DiscourseDeps
       , MonadDiscourseClient (..)
       , MonadDiscourseClientRaw (..)
       , withDiscourseClient

       -- * For tests
       , withDiscourseClientRaw
       , discourseClient
       , topicParser
       ) where

import Control.Concurrent.STM.TBChan (TBChan, newTBChan, readTBChan, tryWriteTBChan)
import Control.Monad.Combinators as MC (between, many, skipManyTill)
import Control.Monad.Reader (withReaderT)
import qualified Data.Map as Map
import Fmt ((+|), (|+))
import Loot.Log (Logging, logDebug, logError)
import Monad.Capabilities (CapImpl(..), CapsT, HasCap, HasCaps, HasNoCap, addCap, makeCap)
import Text.Megaparsec (Parsec, parse)
import Text.Megaparsec.Char (alphaNumChar, printChar, space)
import qualified Text.Megaparsec.Char.Lexer as L
import UnliftIO (MonadUnliftIO)
import qualified UnliftIO as UIO

import Agora.CapUtil
import Agora.Config
import Agora.Discourse.API
import Agora.Discourse.Html
import Agora.Discourse.Types
import Agora.Metrics.Capability
import Agora.State.Capability
import Agora.Types
import Agora.Util

-- Rationale for having this capability in addition to DiscourseClient capability.
-- ==============================================================================
--
-- Originally we had the withDiscourseClient function
-- that implements the Discourse capability. And this accepted a
-- DiscourseEndpointsIO as an argument, and implemented the discourse capabilty
-- using the IO functions in it. This function uses a bracket pattern to launch
-- an async thread to wait on post submission request from the backend.
--
-- Because of this, I was not able to find a way to replace the discourseIO
-- function with a different mock, when running some specific tests. Because
-- the raw capability is not available to use with the `overrideCap` function
-- from caps library.
--
-- Hence, the withDiscourseClient function was changed to fetch the
-- `DiscourseEndpointsIO` from another low level capability, represeted
-- by the following capability.  And in the `withDiscourseClient` function,
-- each method was changed to fetch `DiscourseEndpointsIO` withing that method,
-- instead of using a shared top level value, so that if we replace it with a
-- mock, the correct mock methods will be used. So in short, this enables us
-- to replace the discourseIO function with mocks anytime during a test.
data DiscourseClientRaw m = DiscourseClientRaw
  { _getDiscourseClientRaw :: m DiscourseEndpointsIO
  , _putDiscourseClientRaw :: DiscourseEndpointsIO -> m ()
  }

makeCap ''DiscourseClientRaw

data DiscourseClient m = DiscourseClient
  { _postProposalStubAsync :: ProposalHash -> m ()
  , _getProposalTopic      :: ProposalHash  -> m (Maybe TopicOnePost)
  , _getDiscourseTopic     :: DiscourseTopicId -> m Topic
  , _getDiscoursePost      :: DiscoursePostId -> m Post
  }

makeCap ''DiscourseClient

data DiscourseError
  = CategoryNotFound !Text
  | MultipleTopicMatch !ProposalHash
  deriving (Eq, Show, Generic)

instance Exception DiscourseError

withDiscourseClientRaw
  :: forall caps m a. (HasNoCap DiscourseClientRaw caps, HasAgoraConfig caps, MonadUnliftIO m)
  => CapsT (DiscourseClientRaw ': caps) m a
  -> CapsT caps m a
withDiscourseClientRaw action = do
  host <- fromAgoraConfig $ sub #discourse . option #host
  env <- liftIO $ getClientEnv host
  let discourseEndpointsIO = getDiscourseReal env
  withReaderT (addCap (discourseRawCap discourseEndpointsIO)) action
  where
    discourseRawCap :: DiscourseEndpointsIO -> CapImpl DiscourseClientRaw '[] m
    discourseRawCap ep = CapImpl $ DiscourseClientRaw (pure ep) (\_ -> pure ())

withDiscourseClient
  :: forall m caps a .
  ( HasNoCap DiscourseClient caps
  , HasAgoraConfig caps
  , HasCap Logging caps
  , HasCap DiscourseClientRaw caps
  , HasCap MetricsCap caps
  , HasCap StateCap caps
  , MonadUnliftIO m
  )
  => CapsT (DiscourseClient ': caps) m a
  -> CapsT caps m a
withDiscourseClient action = do
  categoryName <- fromAgoraConfig $ sub #discourse . option #category
  DiscourseEndpointsIO {..} <- getDiscourseClientRaw
  CategoryList categories <- runHTTPIOWithRetry deGetCategoriesIO
  Category{..} <- find ((categoryName ==) . cName) categories
    `whenNothing` UIO.throwIO (CategoryNotFound categoryName)

  chan <- UIO.atomically $ newTBChan 100
  UIO.withAsync (workerPoster chan cId) $ \_ ->
      withReaderT (addCap $ discourseClient chan cId) action

topicParser :: Parsec Void Text String
topicParser = skipManyTill printChar parseTopicHash
  where
    parseTopicHash :: Parsec Void Text String
    parseTopicHash =
      (between (symbol "(") (symbol ")") (MC.many alphaNumChar)) <|>
      (between (symbol "[") (symbol "]") (MC.many alphaNumChar))
    symbol = L.symbol @_ @Text space

type DiscourseDeps = '[AgoraConfigCap, MetricsCap, DiscourseClientRaw, Logging]

discourseClient
  :: forall m . MonadUnliftIO m
  => TBChan ProposalHash
  -> DiscourseCategoryId
  -> CapImpl DiscourseClient DiscourseDeps m
discourseClient chan catId = CapImpl $ DiscourseClient
  { _postProposalStubAsync = \ph -> do
      DiscourseEndpointsIO {..} <- getDiscourseClientRaw
      success <- UIO.atomically $ tryWriteTBChan chan ph
      if success then
        logDebug $ "Task to create a stub topic for " +| shortenHash ph |+ " is added to the Discourse worker queue"
      else
        sendMetrics $ CriticalError $ "Task to create a stub topic for " +| shortenHash ph |+ " is NOT added to the Discourse worker queue"
  , _getProposalTopic = \ph -> do
      matchedTopics <- traversePages [] ph 0
      case matchedTopics of
        [] -> pure Nothing
        -- If one and  only one topic matched with the proposal
        -- proceed. Or else throw and error.
        [topicHead] -> do
          DiscourseEndpointsIO {..} <- getDiscourseClientRaw
          topic <- runHTTPIOWithRetry $ deGetTopicIO (thId topicHead)
          pure $ Just $ convertTopic topic
        _ -> UIO.throwIO (MultipleTopicMatch ph)
  , _getDiscourseTopic = \tid -> do
        DiscourseEndpointsIO {..} <- getDiscourseClientRaw
        runHTTPIOWithRetry $ deGetTopicIO tid
  , _getDiscoursePost = \pid -> do
        DiscourseEndpointsIO {..} <- getDiscourseClientRaw
        runHTTPIOWithRetry $ deGetPostIO pid
  }
  where
    -- Given a hash and a topic title, see if the topic
    -- title contains a bracket enclosed prefix, of the given hash.
    searchHash :: ProposalHash -> Title -> CapsT caps m Bool
    searchHash ph title = case parse topicParser "" (unTitle title) of
      Right topicHash -> pure $ isPrefixOf topicHash (toString $ hashToText ph)
      Left _ -> pure False
    -- Walk through all the discourse topics in the given category
    -- and collect the ones that matches with the proposal hash.
    traversePages :: HasCaps DiscourseDeps caps =>[TopicHead] -> ProposalHash -> Int -> CapsT caps m [TopicHead]
    traversePages thIn ph page = do
      DiscourseEndpointsIO {..} <- getDiscourseClientRaw
      CategoryTopics{..} <- runHTTPIOWithRetry $ deGetCategoryTopicsIO catId (Just page)
      pageTpcs <- filterM (searchHash ph . thTitle) ctTopics
      if length ctTopics < ctPerPage || ctPerPage == 0
        then pure (thIn <> pageTpcs) else traversePages (thIn <> pageTpcs) ph (page + 1)

    -- TODO maybe more robust scheme is needed here
    convertTopic :: Topic -> TopicOnePost
    convertTopic (MkTopic tId tTitle (post :| _)) = MkTopic tId tTitle post

workerPoster
  :: ( MonadUnliftIO m
     , HasAgoraConfig caps
     , HasCap Logging caps
     , HasCap DiscourseClientRaw caps
     , HasCap MetricsCap caps
     , HasCap StateCap caps
     )
  => TBChan ProposalHash
  -> DiscourseCategoryId
  -> CapsT caps m ()
workerPoster chan cId = forever $ do
    ph <- UIO.atomically $ readTBChan chan
    (Map.lookup ph . asProposalCreationFailures) <$> getState >>= \case
      Just err -> logDebug $ "Not retrying proposal creation for proposal hash, " +| ph |+ " since it  has failed in past with error:" +| err |+ ""
      _ -> do
        let shorten = shortenHash ph
        let title = Title $ "(" <> shorten <> ")"
        let body = RawBody $ defaultDescription shorten
        let ct = CreateTopic title body cId
        UIO.catch @_ @SomeException (workerDo ct) (\e -> do
          let
            errorStr = (toText $ displayException e) <> " while trying to create :" <> (show ct)
          modifyAgoraState (\as -> as { asProposalCreationFailures = Map.insert ph errorStr (asProposalCreationFailures as) })
          logError $
            "Something went wrong in the Discourse worker: " +| errorStr |+ "")
    where
      workerDo ct = do
        DiscourseEndpointsIO {..} <- getDiscourseClientRaw
        apiUsername <- fromAgoraConfig $ sub #discourse . option #api_username
        apiKey <- fromAgoraConfig $ sub #discourse . option #api_key
        void $ runHTTPIOWithRetry $ dePostTopicIO (Just apiUsername) (Just apiKey) ct
