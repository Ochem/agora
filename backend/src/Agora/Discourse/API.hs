{-# LANGUAGE DataKinds #-}

{-|
Servant type-level specification for Client API to retrieve data from Tezos Node.
-}
module Agora.Discourse.API
       ( DiscourseEndpoints (..)
       , DiscourseEndpointsIO (..)
       , getDiscourseReal
       ) where

import Servant.API (Capture, Get, Header, JSON, Post, QueryParam, ReqBody, (:>))
import Servant.API.Generic ((:-))
import Servant.Client (ClientEnv, ClientM)
import Servant.Client.Generic (genericClient)

import qualified Agora.Discourse.Types as T
import Agora.Types
import Agora.Util (ApiKey, ApiUsername, runClientM_)

data DiscourseEndpoints route = DiscourseEndpoints
  { dePostTopic :: route
      :- "posts"
      :> Header "Api-Username" ApiUsername
      :> Header "Api-Key" ApiKey
      :> ReqBody '[JSON] T.CreateTopic
      :> Post '[JSON]    T.CreatedTopic

  , deGetCategoryTopics :: route
      :- "c"
      :> Capture "category_id" DiscourseCategoryId
      :> QueryParam "page" Int
      :> Get '[JSON] T.CategoryTopics

  , deGetCategories :: route
      :- "categories"
      :> Get '[JSON] T.CategoryList

  , deGetTopic :: route
      :- "t"
      :> Capture "topic_id" DiscourseTopicId
      :> Get '[JSON] T.Topic

  , deGetPost :: route
      :- "posts"
      :> Capture "post_id" DiscoursePostId
      :> Get '[JSON] T.Post
  } deriving Generic

data DiscourseEndpointsIO = DiscourseEndpointsIO
  { dePostTopicIO :: Maybe ApiUsername -> Maybe ApiKey -> T.CreateTopic -> IO T.CreatedTopic
  , deGetCategoryTopicsIO :: DiscourseCategoryId -> Maybe Int -> IO T.CategoryTopics
  , deGetCategoriesIO :: IO T.CategoryList
  , deGetTopicIO :: DiscourseTopicId -> IO T.Topic
  , deGetPostIO :: DiscoursePostId -> IO T.Post
  }

getDiscourseReal :: ClientEnv -> DiscourseEndpointsIO
getDiscourseReal env =
  let
    DiscourseEndpoints {..} = genericClient
    runEndpoint :: forall a. ClientM a -> IO a
    runEndpoint =
        runClientM_ env
  in DiscourseEndpointsIO
    { dePostTopicIO = \u k t -> runEndpoint (dePostTopic u k t)
    , deGetCategoryTopicsIO = \cid p -> runEndpoint (deGetCategoryTopics cid p)
    , deGetCategoriesIO = runEndpoint deGetCategories
    , deGetTopicIO = \t -> runEndpoint (deGetTopic t)
    , deGetPostIO = \p -> runEndpoint (deGetPost p)
    }
