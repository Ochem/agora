module Agora.BakerFetch.Types
  ( BakerInfo(..)
  , BakerInfoList(..)
  ) where

import Prelude hiding (cycle, id)

import Data.Aeson (FromJSON (..), ToJSON (..))
import Data.Aeson.Options (defaultOptions)
import Data.Aeson.TH (deriveJSON)

import Agora.Types

-- | This is an internal interim type to aid with the parsing of baker info
-- from what is returned from the `tzkt.io` api, which is directly parsed into
-- this type.
data BakerInfoTzKt = BakerInfoTzKt
  { btName :: Text
  , btAddress :: PublicKeyHash
  , btLogo :: Maybe Text
  } deriving (Show, Eq, Generic)

-- | Info about baker
data BakerInfo = BakerInfo
  { biBakerName      :: Text
  , biDelegationCode :: PublicKeyHash
  , biLogo           :: Maybe Text
  , biVoting         :: Maybe Text
  } deriving (Show, Eq, Generic)

toBakerInfoTzkt :: BakerInfo -> BakerInfoTzKt
toBakerInfoTzkt BakerInfo {..} =
  BakerInfoTzKt
    { btName = biBakerName
    , btAddress = biDelegationCode
    , btLogo = biLogo
    }

fromBakerInfoTzkt :: BakerInfoTzKt -> BakerInfo
fromBakerInfoTzkt BakerInfoTzKt {..} =
  BakerInfo
    { biBakerName = btName
    , biDelegationCode = btAddress
    , biLogo = btLogo
    , biVoting = Just $ "https://tzkt.io/" <> (hashToText btAddress) <>"/operations/voting"
    }

-- | Wrapper around the list of @BakerInfo@.
newtype BakerInfoList = BakerInfoList
  { bilBakers :: [BakerInfo]
  } deriving (Show, Eq, Generic)

deriveJSON defaultOptions ''BakerInfoTzKt

instance FromJSON BakerInfo where
  parseJSON v = fromBakerInfoTzkt <$> parseJSON v

instance ToJSON BakerInfo where
  toJSON = toJSON . toBakerInfoTzkt

instance FromJSON BakerInfoList where
  parseJSON v = BakerInfoList <$> parseJSON v

instance ToJSON BakerInfoList where
  toJSON (BakerInfoList l) = toJSON l
