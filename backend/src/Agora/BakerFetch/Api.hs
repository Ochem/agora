-- | Servant type-level specification for the baker fetch client API.

module Agora.BakerFetch.Api
  ( BakerFetchEndpoints (..)
  , BakerFetchCap (..)
  , BakerFetchDeps
  , MonadBakerFetchCap (..)
  , BakerFetchEndpointsIO (..)
  , bakerFetchCap
  , withBakerFetch
  , getBakerFetchReal
  ) where

import Control.Monad.Reader (withReaderT)
import Loot.Log (Logging)
import Monad.Capabilities (CapImpl(..), CapsT, HasCaps, HasNoCap, addCap, makeCap)
import Servant.API (Get, JSON, (:>))
import Servant.API.Generic ((:-))
import Servant.Client (ClientEnv)
import Servant.Client.Generic (genericClient)
import UnliftIO (MonadUnliftIO)

import Agora.BakerFetch.Types
import Agora.CapUtil
import Agora.Config.Definition
import Agora.Metrics.Capability
import Agora.Util
import qualified Agora.Types as AT

data BakerFetchEndpoints route = BakerFetchEndpoints
  { bfBakers :: route
      :- "bakers"
      :> Get '[JSON] BakerInfoList
  } deriving Generic

data BakerFetchEndpointsIO = BakerFetchEndpointsIO
  { bfGetBakers :: IO BakerInfoList
  }

data BakerFetchCap m = BakerFetchCap
  { _getBakers :: m BakerInfoList
  }

makeCap ''BakerFetchCap

bakerFetchCap :: MonadUnliftIO m => BakerFetchEndpointsIO -> CapImpl BakerFetchCap BakerFetchDeps m
bakerFetchCap BakerFetchEndpointsIO {..} = CapImpl $ BakerFetchCap
  { _getBakers = do
      bakers <- runHTTPIOWithRetry bfGetBakers
      predefinedBakers <- fromAgoraConfig $ option #predefined_bakers
      pure $ BakerInfoList ((bilBakers bakers) <> predefinedBakers)
  }

type BakerFetchDeps = '[Logging, MetricsCap, AgoraConfigCap]

type HasBakerFetchDeps caps = (HasCallStack, HasCaps BakerFetchDeps caps)

getBakerFetchReal
  :: (MonadUnliftIO m, HasBakerFetchDeps caps)
  => ClientEnv
  -> CapsT caps m BakerFetchEndpointsIO
getBakerFetchReal env = do
  let BakerFetchEndpoints {..} = genericClient
  pure $ BakerFetchEndpointsIO
    { bfGetBakers = runClientM_ env bfBakers }

withBakerFetch
  :: forall a caps. (HasNoCap BakerFetchCap caps, HasBakerFetchDeps caps)
  => BakerFetchEndpointsIO
  -> CapsT (BakerFetchCap ': caps) AT.Base a
  -> CapsT caps AT.Base a
withBakerFetch bakerFetchIO action = do
  withReaderT (addCap (bakerFetchCap bakerFetchIO)) action
