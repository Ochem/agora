{-|
API handlers implementation.
-}
module Agora.Web.Handlers
       ( agoraHandlers
       , metricsHandler

       -- * For tests
       , getBallots
       , getNonVoters
       , getPeriodInfo
       , getProposal
       , getProposalVotes
       , getProposals
       , getSpecificProposalVotes
       ) where

import qualified Data.List.NonEmpty as NE
import qualified Data.Set as Set
import Data.Time.Clock (UTCTime)
import Prometheus (exportMetricsAsText)
import Servant.API.Generic (ToServant)
import Servant.Server.Generic (AsServerT, genericServerT)

import qualified Agora.Indexer.Handlers as IA
import Agora.Metrics.Capability
import Agora.Mode
import Agora.Types
import Agora.Util
import Agora.Web.API
import Agora.Web.Types
import qualified Agora.Web.Types as T

type AgoraHandlers m = ToServant AgoraEndpoints (AsServerT m)
type MetricsHandler m = ToServant MetricsEndpoint (AsServerT m)

-- | Server handler implementation for Agora API.
agoraHandlers :: forall m . AgoraWorkMode m => AgoraHandlers m
agoraHandlers = genericServerT AgoraEndpoints
  { aePeriod                = getPeriodInfo
  , aeProposals             = getProposals
  , aeProposal              = getProposal
  , aeSpecificProposalVotes = getSpecificProposalVotes
  , aeProposalVotes         = getProposalVotes
  , aeBallots               = getBallots
  , aeNonVoters             = getNonVoters
  }

metricsHandler :: MetricsHandler IO
metricsHandler = genericServerT MetricsEndpoint
  { meMetrics = MetricsResponse <$> exportMetricsAsText
  }

data PeriodStarts = PeriodStarts
  { psPeriodId :: PeriodId
  , psStartTime :: UTCTime
  , psPeriodType :: PeriodType
  , psPeriodSize :: Level
  , psLevelsLeft :: Level
  } deriving Show

-- | Fetch info about specific period.
-- If Nothing passed the last known period will be used.
getPeriodInfo :: forall m. AgoraWorkMode m => Maybe PeriodId -> m PeriodInfo
getPeriodInfo = IA.getPeriodInfo

-- | Get all proposals for passed period.
getProposals
  :: AgoraWorkMode m
  => PeriodId
  -> m [T.Proposal]
getProposals = IA.getProposals

-- | Get info about proposal by proposal id.
getProposal
  :: AgoraWorkMode m
  => Text
  -> m T.Proposal
getProposal propHash = IA.getProposal (encodeHash propHash)

-- | Fetch proposal votes for a proposal
getSpecificProposalVotes
  :: AgoraWorkMode m
  => Text
  -> Maybe ProposalVoteId
  -> Maybe Limit
  -> m (PaginatedList T.ProposalVote)
getSpecificProposalVotes propId mLastId mLimit =
  buildPaginatedList mLimit (fromIntegral <$> mLastId) (fromIntegral . _pvId)
    <$> (IA.getSpecificProposalVotes (encodeHash propId))

-- | Fetch proposal votes for period according to pagination params.
getProposalVotes
  :: AgoraWorkMode m
  => PeriodId
  -> Maybe ProposalVoteId
  -> Maybe Limit
  -> m (PaginatedList T.ProposalVote)
getProposalVotes periodId mLastId mLimit =
  buildPaginatedList mLimit (fromIntegral <$> mLastId) (fromIntegral . _pvId)
    <$> (IA.getProposalVotes periodId)

-- | Fetch ballots for period according to pagination params.
getBallots
  :: AgoraWorkMode m
  => PeriodId
  -> Maybe BallotId
  -> Maybe Limit
  -> Maybe [Decision]
  -> m (PaginatedList T.Ballot)
getBallots periodId mLastId mLimit mDecs = do
  r <- IA.getBallots periodId
  let final = case mDecs of
        Just (Set.fromList -> dcs) -> filter (\b -> Set.member (_bDecision b) dcs) r
        Nothing -> r
  pure $ buildPaginatedList mLimit (fromIntegral <$> mLastId) (fromIntegral . _bId) final

-- Build a Paginated list.Sorts the items in descending order using the  provided function.
buildPaginatedList :: Maybe Limit -> Maybe Word32 -> (b -> Word32) -> [b] -> PaginatedList b
buildPaginatedList mLimit mLast fn items = let
  paired = zip items (fn <$> items)
  sorted = sortBy (\a b -> compare (snd b) (snd a) ) paired
  totalLength = length items
  limit = case mLimit of
    Just lm -> lm
    Nothing -> fromIntegral totalLength
  fromLast = case mLast of
    Just lst -> dropWhile ((>= lst) . snd) sorted
    Nothing -> sorted
  final = take (fromIntegral limit) fromLast
  rest = length fromLast - length final
  paginatedData = PaginationData (fromIntegral rest) mLimit ((snd . last) <$> NE.nonEmpty final)
  in PaginatedList paginatedData (fst <$> final)

-- | Fetch nonVoters for period
getNonVoters :: AgoraWorkMode m => PeriodId -> m [T.Baker]
getNonVoters pid =
  (plResults . buildPaginatedList Nothing Nothing (fromIntegral . _bkRolls) <$> (IA.getNonVoters pid))
