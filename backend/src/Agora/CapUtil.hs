module Agora.CapUtil
  ( runHTTPIOWithRetry
  ) where

import Data.List (lookup)
import Data.Time.Units (Second)
import Fmt ((+|), (|+))
import Loot.Log (Logging, logWarning)
import Monad.Capabilities (CapsT, HasCaps)
import Servant.Client (responseHeaders)
import Servant.Client.Streaming (ClientError(..))
import Text.Read
import UnliftIO (MonadUnliftIO)
import qualified UnliftIO as UIO

import Agora.Metrics.Capability
import Agora.Util

runHTTPIOWithRetry
  :: forall a m caps. (MonadUnliftIO m, HasCaps '[MetricsCap, Logging] caps)
  => IO a
  -> CapsT caps m a
runHTTPIOWithRetry act = do
  suppressException 30 handler (liftIO act)
  where
    handler :: ClientError -> CapsT caps m ()
    handler ce = do
      getRetryAfter ce >>= \case
        Just x -> do
          logWarning ("Recoverable error:" +| (show @Text ce) |+ " happend in HTTP request, retrying...")
          waitFor x
        Nothing -> do
          sendMetrics (Warning $ "Unrecoverable error:" <> (show ce) <> " happend in HTTP request.")
          UIO.throwIO ce

    -- Only retry in case of connection error or when server responds with
    -- an explicit retry-after value.
    getRetryAfter :: ClientError -> CapsT caps m (Maybe Second)
    getRetryAfter (ConnectionError _) = pure $ Just 30
    getRetryAfter (FailureResponse _ resp) = pure $
      (fromIntegral . read @Integer . decodeUtf8) <$> (lookup "Retry-After" (toList $ responseHeaders resp))
    getRetryAfter _ = pure Nothing
