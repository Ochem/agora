{-# LANGUAGE DeriveAnyClass #-}

-- | This module export a capability that lets us log monitoring info as well as
-- send associated metrics to the Prometheus service.
module Agora.Metrics.Capability
  ( MetricsEvent(..)
  , MetricsCap(..)
  , MonadMetricsCap(..)
  , MetricsResponse(..)
  , withMetricsCap
  ) where

import Control.Monad.Reader (withReaderT)
import qualified Data.ByteString.Lazy as BSL
import Fmt (Buildable(..), (+|), (|+))
import Loot.Log (Logging, logError, logWarning)
import Monad.Capabilities (CapImpl(..), CapsT, HasCap, HasNoCap, addCap, makeCap)
import Prometheus (Counter, Gauge, Info(..), counter, gauge, incCounter, register, setGauge)
import Servant (MimeRender, OctetStream)
import Servant.Util (ForResponseLog(..))
import UnliftIO (MonadUnliftIO)

import qualified Agora.Types as AT

-- | A type to represent the events
data MetricsEvent
  = ProcessedBlock AT.Level
  | Warning Text
  | CriticalError Text
  deriving stock (Show, Generic)

newtype MetricsResponse = MetricsResponse BSL.ByteString
  deriving newtype (MimeRender OctetStream)
  deriving Generic

instance Buildable MetricsResponse where
  build (MetricsResponse x) = build @Text $ decodeUtf8 x

deriving newtype instance Buildable (ForResponseLog MetricsResponse)

-- | A capability to expose adding and reading the metrics and errors
data MetricsCap m = MetricsCap
  { _sendMetrics :: MetricsEvent -> m ()
  }

makeCap ''MetricsCap

data Metrics = Metrics
  { mProcessedBlock     :: Gauge
  , mWarningCount       :: Counter
  , mCriticalErrorCount :: Counter
  }

-- This function can accept instances of various (Right now there is only one)
-- 'Metric'  (created by a combination of `register` function and various
-- metric creation functions described in
--
-- https://hackage.haskell.org/package/prometheus-client-1.0.1/docs/Prometheus.html#g:3
--
-- and implements logic to modfiy them based on the metric events we receive
-- from the capability method 'sendMetrics' calls.
--
-- The `register` function appear to add these Metrics to some kind of global
-- mutable storage and thus we can later collect the updated Metrics using
-- another IO call to 'exportMetricsAsText' Which can be seen in the
-- implementation of the handler for `/metric` endpoint.
metricsCap :: (MonadUnliftIO m) => Metrics -> CapImpl MetricsCap '[Logging] m
metricsCap Metrics {..} = CapImpl $ MetricsCap
  { _sendMetrics = \case
      ProcessedBlock (realToFrac -> level) ->
        liftIO $ setGauge mProcessedBlock level
      Warning msg -> do
        logWarning ("Warning:" +| msg |+ ".")
        liftIO $ incCounter mWarningCount
      CriticalError msg -> do
        logError ("Critical error:" +| msg |+ ".")
        liftIO $ incCounter mCriticalErrorCount
  }

withMetricsCap
  :: forall m caps a .
  ( MonadUnliftIO m
  , HasNoCap MetricsCap caps
  , HasCap Logging caps
  )
  => CapsT (MetricsCap ': caps) m a
  -> CapsT caps m a
withMetricsCap action = do
  blocksCounter <- liftIO $ register $ gauge (Info "processed_blocks" "Processed block gauge")
  warningCounter <- liftIO $ register $ counter (Info "warnings" "Warning occurance count")
  criticalErrorCounter <- liftIO $ register $ counter (Info "critical_error" "Critical error occurance count")
  let metrics = Metrics
        { mProcessedBlock = blocksCounter
        , mWarningCount = warningCounter
        , mCriticalErrorCount = criticalErrorCounter
        }
  withReaderT (addCap $ metricsCap metrics) action
