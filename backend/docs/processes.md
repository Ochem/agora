# Process documentation

When agora process starts, it starts the following processes:

1. The agora web server
2. The process that fetches new blocks from the node
3. The process that monitor proposals from db and syncs them with discourse
4. The process that does a fetch of bakers

Processes 1 and 2 are started from `runAgora` function in `Agora.Web.Server` module.

The process that fetches new blocks from node inserts new proposals to the db
when it encounters a block with a proposal. At that point all the discourse related
attributes are null.

The third process, (implemented in `syncProposals` function in `Agora.Discourse.Client`)
which runs periodically, queries all the proposals from the db.

The rows with discourse fields set and rows without them are handled differently, as follows.

For each proposal, P, in the result which does not have discourse fields set, the following process is executed.

  1. Fetch all topics from discourse under the category `Proposals` (the name comes from config.yaml).
  and filter out the ones that do not match with the proposal hash for P. The proposal matching
  is done by searching for a continuous sequence of alpha numeric characters immedietly
  surrounded by paranthesis ('(' or '['), and checking if that sequence is a prefix of proposal
  hash of P.

  2. If the result of step 1 was exactly one item, then it is considered to be the topic
  that matches with this proposal (P), and we proceed to parse the topic meta data and fill
  the proposal P's discourse fields with the parsed data. If the result of the process was empty list
  we just trigger the creation of the topic in discourse for the proposal. Then we delegate the
  followup process to the next iteration of the `syncProposals` function. If the result of the
  filtering was a list with more then one item, then we throw an exception.

For each proposal, P, in the result which have discourse fields set, the following process is executed:

  1. Fetch the Discourse post using its stored topic/post id.
  2. If that request failed, we assume that the topic is no longer available, and we clear all the
     discourse fields from the db. We delagate the filling of the columns with proper values to the
     next iteration of `syncProposals`.
  3. If that request was successful, we parse the content of the topic/post and update the existing
     metadata columns for the proposal P in the db.

Note that we need a valid discourse api key in the config for the topic creation to work. But the
topic association process will work without a valid key, as the concerned endpoints are read only, thus
require no authentication.
