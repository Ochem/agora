# SPDX-FileCopyrightText: 2020 Tocqueville Group
# SPDX-License-Identifier: AGPL-3.0-or-later

{ self }@args:

{ config, pkgs, lib, ... }:
let
  cfg = config.services.agora;
in {
  imports = [ (import ./frontend.nix args) (import ./backend.nix args) ];
  options.services.agora = {
    enable = lib.mkEnableOption "Agora frontend and backend combined with some sane defaults";
  };
  config = lib.mkIf cfg.enable {
    services.agora.frontend = {
      enable = true;
      api_addr = "http://${config.services.agora.backend.config.api.listen_addr}";
      fqdn = lib.mkDefault "${config.networking.hostName}.${config.networking.domain}";
    };
    services.agora.backend = {
      enable = true;
      configurePostgres = true;
    };
  };
}
