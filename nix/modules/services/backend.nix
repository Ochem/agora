# SPDX-FileCopyrightText: 2020 Tocqueville Group
# SPDX-License-Identifier: AGPL-3.0-or-later

{ self }:

{ config, lib, pkgs, ... }:

let
  packages = self.packages.${config.nixpkgs.system};
  description = "Tezos Agora, an explorer for Tezos voting system";
  userDescription = "The user that runs Tezos Agora";
  cfg = config.services.agora.backend;

  # Throw if a config value is unset
  # We can't just do `unsetValue = path: throw ...` because recursiveUpdate
  # will lazily evaluate it and that results in a throw. If we add a proxy
  # attrset, lazily evaluating it doesn't throw, but deeply evaluating it
  # (which toJSON does) throws.
  unsetValue = path: {
    fakeValue = throw "You haven't set services.agora.backend.config.${
        builtins.concatStringsSep "." path
      }";
  };

  # Throw if a config value is null
  throwForNulls = lib.mapAttrsRecursive
    (path: value: if isNull value then unsetValue path else value);

  # Base config (backend/config.yaml)
  baseConfig = builtins.fromJSON (builtins.readFile
    (pkgs.runCommand "config.json" { buildInputs = [ pkgs.yaml2json ]; }
      "cat ${packages.agora-backend-config}/base-config.yaml | yaml2json > $out"));

  renderConfigJSON = config:
    pkgs.writeText "config.json" (builtins.toJSON config);
in {

  options = {
    services.agora.backend = {
      enable = lib.mkEnableOption description;

      package = lib.mkOption {
        type = lib.types.path;
        default = packages.agora-backend;
      };

      configurePostgres = lib.mkOption {
        type = lib.types.bool;
        default = false;
        description = "Whether to configure a postgresql database for agora";
      };

      user = lib.mkOption {
        type = lib.types.str;
        default = "agora";
        description = userDescription;
      };

      config = lib.mkOption rec {
        type = lib.types.attrs;
        default = lib.recursiveUpdate baseConfig {
          node_addr = "https://mainnet-tezos.giganode.io:443";
          api = {
            listen_addr = "127.0.0.1:24672";
            serve_docs = false;
          };
          db = { conn_string = null; };
          discourse = {
            host = null;
            category = null;
            api_username = null;
            api_key = "$DISCOURSE_API_KEY";
          };
        };
        apply = lib.recursiveUpdate (throwForNulls default);
      };

      secretFile = lib.mkOption {
        type = lib.types.nullOr lib.types.path;
        default = null;
        description = ''
          File containing `source`-able secrets
          - DISCOURSE_API_KEY : required unless config.discourse.api_token is set
          You can pass other secrets to the config by using `$SECRET_NAME`
        '';
      };
    };
  };

  config = {
    systemd.services.agora = rec {
      inherit description;
      wantedBy = [ "multi-user.target" ];

      after = [ "postgresql.service" "network-online.target" ];
      requires = after;

      path = [ cfg.package pkgs.envsubst ];

      script = ''
        agora -c <(envsubst -i ${
          renderConfigJSON cfg.config
        } -no-unset -no-empty)
      '';

      serviceConfig = {
        PrivateTmp = true;
        User = cfg.user;
        EnvironmentFile = lib.mkIf (!isNull cfg.secretFile) cfg.secretFile;
      };
    };

    services.agora.backend.config.db.conn_string = if cfg.configurePostgres then
      "host=/run/postgresql dbname=agora"
    else
      null;

    services.postgresql = lib.mkIf cfg.configurePostgres {
      enable = true;
      ensureUsers = [{
        name = cfg.user;
        ensurePermissions = { "DATABASE agora" = "ALL PRIVILEGES"; };
      }];
      ensureDatabases = [ "agora" ];
    };

    users.users.${cfg.user} = { isSystemUser = true; description = userDescription; };

  };

}
