let self = (import (fetchTarball https://github.com/edolstra/flake-compat/archive/master.tar.gz) {
  src = ./.;
}).defaultNix; in
self // self.packages.${builtins.currentSystem} // self.checks.${builtins.currentSystem}
